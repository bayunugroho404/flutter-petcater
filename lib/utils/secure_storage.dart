import 'package:flutter_secure_storage/flutter_secure_storage.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2021 . All rights reserved.
 */

class SecureStorage {
  static SecureStorage? _instance;

  factory SecureStorage() =>
      _instance ??= SecureStorage._(FlutterSecureStorage());

  SecureStorage._(this._storage);

  final FlutterSecureStorage _storage;
  static const _tokenKey = 'TOKEN';
  static const _roleKey = 'ROLE';
  static const _nameKey = 'NAME';
  static const _idKey = 'ID';
  static const _emailKey = 'EMAIL';
  static const _addressKey = 'ADDRESS';

  Future<void> persistEmailAndToken(String email, String token) async {
    await _storage.write(key: _emailKey, value: email);
    await _storage.write(key: _tokenKey, value: token);
  }

  Future<void> persistUser(String token,String email,String id,String roleUser,String name,String address) async {
    await _storage.write(key: _tokenKey, value: token);
    await _storage.write(key: _emailKey, value: email);
    await _storage.write(key: _idKey, value: id);
    await _storage.write(key: _roleKey, value: roleUser);
    await _storage.write(key: _nameKey, value: name);
    await _storage.write(key: _addressKey, value: address);
  }

  Future<void> updateUser(String email, String name,String address) async {
    await _storage.write(key: _emailKey, value: email);
    await _storage.write(key: _nameKey, value: name);
    await _storage.write(key: _addressKey, value: address);
  }

  Future<bool> hasToken() async {
    var value = await _storage.read(key: _tokenKey);
    return value != null;
  }

  Future<bool> hasEmail() async {
    var value = _storage.read(key: _emailKey);
    return value != null;
  }

  Future<void> deleteToken() async {
    return _storage.delete(key: _tokenKey);
  }

  Future<void> deleteEmail() async {
    return _storage.delete(key: _emailKey);
  }

  Future<String?> getEmail() async {
    return _storage.read(key: _emailKey);
  }
  Future<String?> getName() async {
    return _storage.read(key: _nameKey);
  }

  Future<String?> getRole() async {
    return _storage.read(key: _roleKey);
  }

  Future<String?> getIdUser() async {
    return _storage.read(key: _idKey);
  }

  Future<String?> getToken() async {
    return _storage.read(key: _tokenKey);
  }

  Future<String?> getAddress() async {
    return _storage.read(key: _addressKey);
  }

  Future<void> deleteAll() async {
    return _storage.deleteAll();
  }
}
