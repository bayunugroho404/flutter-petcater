import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:petcater/app/data/remote/api/api_config.dart';
import 'package:petcater/app/data/remote/responses/response_get_laporan.dart';
import 'package:petcater/utils/secure_storage.dart';

class LaporanController extends GetxController {
  final listLaporan = <Datum>[].obs;
  final secure = SecureStorage();
  final count = 0.obs;
  final format = new DateFormat('dd-MMM-yyyy hh:mm');

  @override
  void onInit() {
    getLaporan();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void getLaporan() async{
    String? token = await secure.getToken();
    listLaporan.clear();
    APIService().getLaporan(token!).then((value){
      listLaporan.value = value.data!;
      update();
    }).catchError((_){});
    update();
  }
  String formatDate(DateTime date){
    String dateFormat = format.format(date);
    return dateFormat.toString();
  }
}
