import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:petcater/app/modules/detail_laporan/views/detail_laporan_view.dart';
import 'package:petcater/utils/constant.dart';
import 'package:petcater/utils/size_config.dart';

import '../controllers/laporan_controller.dart';

class LaporanView extends GetView<LaporanController> {
  final controller = Get.put(LaporanController());

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        appBar: AppBar(
      backgroundColor: Colors.white,
      leading: IconButton(
        icon: Icon(Icons.arrow_back_ios,color: primary,),
        onPressed: (){Get.back();},
      ),
      title: Text(
        'Laporan',
        textAlign: TextAlign.left,
        style: GoogleFonts.lato(
          color: grey,
          fontSize: SizeConfig.blockVertical * 3,
        ),
      ),
    ),
        body: Obx(() => controller.listLaporan.length > 0
            ? Container(
          width: SizeConfig.screenWidth,
          child: Expanded(
            child: ListView.builder(
              shrinkWrap: true,
              primary: false,
              scrollDirection: Axis.vertical,
              itemCount: controller.listLaporan.length,
              itemBuilder: (
                  context,
                  index,
                  ) {
                return
                  ExpansionTile(
                    title: Row(
                      children: [
                        Text(
                          "${controller.listLaporan[index].code} || ${controller.formatDate(controller.listLaporan[index].updatedAt!)}",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    subtitle: Row(
                      children: [
                        Text(
                          "${controller.listLaporan[index].transactionStatus}",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Rp.${controller.listLaporan[index].totalPrice}'),
                      ),
                    ElevatedButton(
                      child: Text('Lihat Detail',textAlign: TextAlign.center,),
                      style: ElevatedButton.styleFrom(
                        primary: primary,
                      ),
                      onPressed: () {
                        Get.to(DetailLaporanView(
                          id:controller.listLaporan[index].id.toString(),
                          code:controller.listLaporan[index].code
                        ));
                      },
                    ),
                    ],
                  );
              },
            ),
          ),
        )
            : Center(child: Text('tidak ada laporan')))
    );
  }
}
