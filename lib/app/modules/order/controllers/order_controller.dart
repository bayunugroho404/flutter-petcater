import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:petcater/app/data/remote/api/api_config.dart';
import 'package:petcater/app/modules/home/views/home_view.dart';
import 'package:petcater/app/modules/signin/views/signin_view.dart';
import 'package:petcater/app/modules/widgets/toast.dart';
import 'package:petcater/utils/secure_storage.dart';

class OrderController extends GetxController {
  final secure = SecureStorage();

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void order(String price, BuildContext context)async{
    String? token = await secure.getToken();
    APIService().postTransaction(token!, price).then((value) {
      if(value.success == true){
        showToast("order anda berhasil");
        Get.offAll(HomeView());
        // Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => HomeView()));
      }else{
        showToast("ops, order gagal");
      }
    }).catchError((onError){
      showToast('$onError');
    });
  }
}
