import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:petcater/app/model.dart';
import 'package:petcater/app/modules/widgets/toast.dart';
import 'package:petcater/utils/constant.dart';
import 'package:petcater/utils/size_config.dart';

import '../controllers/order_controller.dart';

class OrderView extends GetView<OrderController> {
  List<CartModel>? listProduct = [];
  String? total;

  OrderView({this.listProduct, this.total});
  final controller = Get.put(OrderController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Order'),
        backgroundColor: primary,
      ),
      body: Container(
        child: Column(
          children: [
            Container(
              width: SizeConfig.screenWidth,
              child: Padding(
                padding: EdgeInsets.only(left: 20, top: 20, bottom: 4),
                child: Text(
                  'Produk Order',
                  textAlign: TextAlign.left,
                  style: GoogleFonts.lato(
                    fontSize: SizeConfig.blockVertical * 3,
                  ),
                ),
              ),
            ),
            Divider(),
            Container(
              width: SizeConfig.screenWidth,
              height: SizeConfig.screenHight / 3,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    listProduct!.length > 0
                        ? Container(
                            width: SizeConfig.screenWidth,
                            child: Expanded(
                              child: ListView.builder(
                                shrinkWrap: true,
                                primary: false,
                                scrollDirection: Axis.vertical,
                                itemCount: listProduct!.length,
                                itemBuilder: (
                                  context,
                                  index,
                                ) {
                                  return Container(
                                    child: Card(
                                      child: Container(
                                        height: SizeConfig.screenHight / 6,
                                        width: SizeConfig.screenWidth,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Flexible(
                                              flex: 1,
                                              child: Container(
                                                child: Image.network(
                                                  listProduct![index].image!,
                                                  fit: BoxFit.fill,
                                                ),
                                              ),
                                            ),
                                            Flexible(
                                              flex:2,
                                              child: Text(
                                                "${listProduct![index].name}, \n(Rp. ${listProduct![index].price})",
                                                style: TextStyle(
                                                    fontSize: SizeConfig
                                                            .blockVertical *
                                                        2,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                  ;
                                },
                              ),
                            ),
                          )
                        : CircularProgressIndicator()
                  ],
                ),
              ),
            ),
            Container(
              width: SizeConfig.screenWidth,
              child: Padding(
                padding: EdgeInsets.only(left: 20, top: 20, bottom: 4),
                child: Text(
                  'Informasi',
                  textAlign: TextAlign.left,
                  style: GoogleFonts.lato(
                    fontSize: SizeConfig.blockVertical * 3,
                  ),
                ),
              ),
            ),
            Divider(),
            Container(
              width: SizeConfig.screenWidth,
              padding: EdgeInsets.only(left: 30, top: 10, bottom: 4,right: 30),
              child: Center(
                child: Text(
                  'Silahkan melakukan transfer ke akun dibawah ini sebesar Rp.$total \n\n BCA 14045, Atung Gunawan',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.lato(
                    fontSize: SizeConfig.blockVertical * 2,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 100),
              child: TextButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(primary),
                  //Background Color
                  elevation: MaterialStateProperty.all(3),
                  //Defines Elevation
                  shadowColor:
                  MaterialStateProperty.all(grey), //Defines shadowColor
                ),
                onPressed: () {
                  showDialogs(context);
                  },
                child: Container(
                    width: SizeConfig.screenWidth / 3,
                    child: Text(
                      'Order',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white),
                    )),
              ),
            ),

          ],
        ),
      ),
    );
  }
  void showDialogs(BuildContext context) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        title: 'total pembayaran Rp.$total',
        btnOkText: "Order",
        desc: 'Apakah anya yakin order ?',
        btnOkOnPress: () {
          controller.order(total!,context);
        },
        btnCancelOnPress: (){}
      )..show();
  }

}
