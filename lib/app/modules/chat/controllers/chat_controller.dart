import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:petcater/app/data/remote/api/api_config.dart';
import 'package:petcater/app/model.dart';
import 'package:petcater/utils/secure_storage.dart';

class ChatController extends GetxController {
  final listChat = <ChatModel>[].obs;
  final secure = SecureStorage();
  final token =''.obs;
  final format = new DateFormat('HH:mm, dd-MMM');
  final format2 = new DateFormat('yyyy-MM-dd HH:mm:ss');
  final userId =''.obs;
  final role = ''.obs;

  @override
  void onInit() {
    super.onInit();
    addChat();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<bool> addChat() async{
    token.value = (await secure.getToken())!;
    userId.value = (await secure.getIdUser())!;
    role.value = (await secure.getRole())!;
    update();
    await APIService().getChat(token.value, role.value == "USER"?"ADMIN":"USER").then((value) {
      listChat.clear();
      value.data!.forEach((element) {
        listChat.add(new ChatModel(
            image:element.photo,
            name: element.name,
            id: element.id,
            os_id: element.os_id,
            date: element.latestMessage == null ?'': formatDate(element.latestMessage!.createdAt.toString()),
            dateSort: element.latestMessage == null ? null : DateTime.parse(element.latestMessage!.createdAt.toString()),
            lastMessage: element.latestMessage == null ?'':element.latestMessage!.message,
            lastChat: element.roles));
      });
      update();
    }).catchError((_){});
    listChat.sort((a,b) => b.date!.compareTo(a.date!));
    update();
    return true;
  }
  String formatDate(String date){
    var parsedDate = DateTime.parse(date);
    String dateFormat = format.format(parsedDate);
    return dateFormat.toString();
  }

  String formatDate2(DateTime date){
    String dateFormat = format2.format(date);
    print(dateFormat);
    return dateFormat.toString();
  }
}
