import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:petcater/app/modules/detail_chat/views/detail_chat_view.dart';
import 'package:petcater/utils/constant.dart';
import 'package:petcater/utils/size_config.dart';

import '../controllers/chat_controller.dart';

class ChatView extends GetView<ChatController> {
  final controller = Get.put(ChatController());

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    controller.onInit();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'Chat',
          textAlign: TextAlign.left,
          style: GoogleFonts.lato(
            color: grey,
            fontSize: SizeConfig.blockVertical * 3,
          ),
        ),
      ),
      body: Obx(() =>
      controller.listChat.length > 0
          ? Container(
        width: SizeConfig.screenWidth,
        child: Expanded(
        child: ListView.builder(
            physics: ClampingScrollPhysics(),
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            primary: false,
            itemCount: controller.listChat.length,
            itemBuilder: (context,
                index,) {
              return ListTile(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              DetailChatView(
                                  name: "${controller.listChat[index]
                                      .name}",
                                  os_id: "${controller.listChat[index]
                                      .os_id}",
                                  id: "${controller.listChat[index].id}"))).then((value) {
                    controller.addChat();
                  });
                },
                leading: Container(
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    backgroundImage: NetworkImage("${
                        controller.listChat[index].image ==
                            '$BASE_URL/storage'
                            ?
                        "https://asset.kompas.com/crops/Ig2iBphZ8bxLL5rOQS8v_fxJscw=/253x25:999x522/750x500/data/photo/2019/05/19/976392325.jpg"
                            :
                        controller.listChat[index].image}"),
                  ),
                ),
                trailing: Obx(()=>Visibility(
                  visible: controller.role == "ADMIN"?true:false,
                  child: Text('${controller.listChat[index].date == null
                      ? ''
                      :
                  controller.listChat[index].date!}'),
                )),
                subtitle: Obx(()=>Visibility(
                  visible: controller.role == "ADMIN"?true:false,
                  child: Text(
                      '${controller.listChat[index].lastMessage == null?'gambar':controller.listChat[index].lastMessage}'),
                )),
                title: Text(
                    "${controller.listChat[index].name}"),
              );
            },
        ),
      ),
          )
          : CircularProgressIndicator()),
    );
  }
}
