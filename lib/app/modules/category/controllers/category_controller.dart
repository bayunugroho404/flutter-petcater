import 'package:get/get.dart';
import 'package:petcater/app/data/remote/api/api_config.dart';
import 'package:petcater/app/model.dart';

class CategoryController extends GetxController {
  final listProductCategory = <ProductCategory>[].obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void getProductByCategory(String id){
    listProductCategory.clear();
    APIService().getProductByCategory(id).then((val){
      if(val.data!.length>0){
        val.data!.forEach((element) {
          listProductCategory.add(new ProductCategory(
              name: element.name,
              id: element.id,
              price: element.price.toString(),
              desc: element.description,
              image: "https://images.tokopedia.net/img/cache/215-square/shops-1/2020/12/20/9492632/9492632_eea5fd7a-e971-4018-9302-9ca9383ee3d2.jpg",
              imageGallery: element.galleries
          ));
        });
        update();
      }
    });
  }
}
