import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:petcater/app/modules/detail_page/views/detail_page_view.dart';
import 'package:petcater/utils/constant.dart';
import 'package:petcater/utils/size_config.dart';

import '../controllers/category_controller.dart';

class CategoryView extends GetView<CategoryController> {
  String? name;
  int? id;

  CategoryView({this.id, this.name});

  final controller = Get.put(CategoryController());

  @override
  Widget build(BuildContext context) {
    controller.getProductByCategory(id.toString());
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(
            Icons.arrow_back_ios,
            color: grey,
          ),
        ),
        backgroundColor: Colors.white,
        title: Text(
          'Category $name',
          textAlign: TextAlign.left,
          style: GoogleFonts.lato(
            color: grey,
            fontSize: SizeConfig.blockVertical * 3,
          ),
        ),
      ),
      body: Container(
        width: SizeConfig.screenWidth,
        child: ListView(
          primary: true,
          children: [
            Obx(() => controller.listProductCategory.length > 0
                ? Container(
              margin: EdgeInsets.only(left: 10, right: 10),
              width: SizeConfig.screenWidth,
              child: GridView.builder(
                shrinkWrap: true,
                primary: false,
                itemCount: controller.listProductCategory.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  mainAxisSpacing: 3.0,
                  childAspectRatio: 1,
                ),
                itemBuilder: (
                    context,
                    index,
                    ) {
                  return Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    elevation: 4,
                    child: GestureDetector(
                      onTap: () {
                        Get.to(DetailPageView(
                            name: controller.listProductCategory[index].name!,
                            price: controller.listProductCategory[index].price!,
                            id: controller.listProductCategory[index].id
                                .toString(),
                            desc: controller.listProductCategory[index].desc,
                            image: controller.listProductCategory[index].image!,
                            source: "category",
                            imageGallery2: controller
                                .listProductCategory[index].imageGallery));
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Flexible(
                            flex: 2,
                            child: Container(
                              decoration: BoxDecoration(
                                  border:
                                  Border.all(color: Colors.white)),
                              child: Image.network(
                                (controller.listProductCategory[index].imageGallery!.length>0?
                                controller.listProductCategory[index].imageGallery![0].photos:
                                controller.listProductCategory[index].image!)!,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                          Flexible(
                            flex: 1,
                            child: Container(
                              width: SizeConfig.screenWidth,
                              child: Text(
                                controller.listProductCategory[index].name! +
                                    "\n" +
                                    "Rp." +
                                    controller
                                        .listProductCategory[index].price!,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.lato(
                                  fontSize:
                                  SizeConfig.blockVertical * 2,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                },
              ),
            )
                : Container(
                width: SizeConfig.screenWidth,
                height: SizeConfig.screenHight,
                child: Center(child: CircularProgressIndicator()))),
          ],
        ),
      ),
    );
  }
}
