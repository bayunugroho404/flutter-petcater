import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:petcater/app/modules/order/views/order_view.dart';
import 'package:petcater/app/modules/widgets/toast.dart';
import 'package:petcater/utils/constant.dart';
import 'package:petcater/utils/size_config.dart';

import '../controllers/cart_controller.dart';

class CartView extends StatefulWidget {
  @override
  _CartViewState createState() => _CartViewState();
}

class _CartViewState extends State<CartView> {
  final controller = Get.put(CartController());

  @override
  void initState() {
    controller.addCart();
    Future.delayed(const Duration(milliseconds: 500), () {
      if(controller.listCart.length >0){
        // controller.countTotal();
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Colors.white,
          actions: [
            Center(
              child: Padding(
                padding: EdgeInsets.only(right: 8.0),
                child: Obx(() =>
                    Text(
                      'Rp. ${controller.total}',
                      textAlign: TextAlign.left,
                      style: GoogleFonts.lato(
                        color: grey,
                        fontSize: SizeConfig.blockVertical * 3,
                      ),
                    )),
              ),
            )
          ],
          title: Text(
            'Keranjang',
            textAlign: TextAlign.left,
            style: GoogleFonts.lato(
              color: grey,
              fontSize: SizeConfig.blockVertical * 3,
            ),
          ),
        ),
        body: ListView(
          primary: true,
          shrinkWrap: true,
          children: [
            ListView(
              primary: false,
              shrinkWrap: true,
              children: [
                Obx(() =>
                controller.listCart.length > 0
                    ? Container(
                  width: SizeConfig.screenWidth,
                  height: SizeConfig.screenHight /1.4,
                  child: ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    scrollDirection: Axis.vertical,
                    itemCount: controller.listCart.length,
                    itemBuilder: (context,
                        index,) {
                      return Container(
                        child: Card(
                          child: Container(
                            height: SizeConfig.screenHight / 6,
                            width: SizeConfig.screenWidth,
                            child: Row(
                              mainAxisAlignment:
                              MainAxisAlignment.start,
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: [
                                Container(
                                  height: 100,
                                  width: 120,
                                  child: Image.network(
                                    controller
                                        .listCart[index].image!,
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment
                                      .center,
                                  crossAxisAlignment: CrossAxisAlignment
                                      .start,
                                  children: [
                                    Text(
                                      "${controller.listCart[index]
                                          .name}, \n(Rp. ${controller
                                          .listCart[index].price})",
                                      style: TextStyle(
                                          fontSize: SizeConfig
                                              .blockVertical * 2,
                                          fontWeight: FontWeight
                                              .bold),),
                                    SizedBox(height: 6,),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment
                                          .start,
                                      crossAxisAlignment: CrossAxisAlignment
                                          .start,
                                      children: [
                                        InkWell(
                                          onTap: () {
                                            controller.desc(
                                                controller
                                                    .listCart[
                                                index]);
                                          },
                                          child: Card(
                                              child: Container(
                                                  margin:
                                                  EdgeInsets
                                                      .all(4),
                                                  child: Icon(
                                                      Icons
                                                          .add))),
                                        ),
                                        Obx(() =>
                                            Padding(
                                              padding:
                                              const EdgeInsets
                                                  .all(8.0),
                                              child: Text(
                                                  "${controller
                                                      .listCart[index]
                                                      .qty}"),
                                            )),
                                        InkWell(
                                          onTap: () {
                                            controller.asc(
                                                controller
                                                    .listCart[
                                                index]);
                                          },
                                          child: Card(
                                              child: Container(
                                                  margin:
                                                  EdgeInsets
                                                      .all(4),
                                                  child: Icon(Icons
                                                      .remove))),
                                        ),
                                      ],
                                    )
                                  ],
                                ),

                              ],
                            ),
                          ),
                        ),
                      );
                      ;
                    },
                  ),
                )
                    : Center(child: Text('tidak ada produk di keranjang')))
              ],
            ),
            Container(
              width: SizeConfig.screenWidth / 2,
              child: TextButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(primary),
                  elevation: MaterialStateProperty.all(3),
                  shadowColor:
                  MaterialStateProperty.all(grey), //Defines shadowColor
                ),
                onPressed: () {
                  if (controller.total.value > 0) {
                    Get.to(OrderView(
                        listProduct: controller.listCart,
                        total: controller.total.toString()
                    ));
                  } else {
                    showToast('gagal');
                  }
                },
                child: Text(
                  'Checkout',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ],
        ));
  }
}


