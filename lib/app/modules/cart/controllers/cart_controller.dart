import 'package:get/get.dart';
import 'package:petcater/app/data/remote/api/api_config.dart';
import 'package:petcater/app/model.dart';
import 'package:petcater/utils/secure_storage.dart';

class CartController extends GetxController {
  final listCart = <CartModel>[].obs;
  final total = 0.obs;
  final qty = 1.obs;
  final secure = SecureStorage();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    total.value = 0;
  }

  void addCart() async {
    listCart.clear();
    String? token = await secure.getToken();
    APIService().getCart(token!).then((val) {
      if (val.data!.length > 0) {
        listCart.clear();
        total.value = 0;
        update();
        val.data!.forEach((element) {
          total.value += (element.product!.price! * element.qty!);
          update();
          listCart.add(new CartModel(
              name: element.product?.name,
              id: element.product?.id,
              qty: element.qty,
              price: element.product?.price,
              image: "https://pbs.twimg.com/media/EUU6kHiUMAAcOZs.png"));
        });
      } else {
        total.value = 0;
        update();
      }
      listCart.toSet().toList();
      update();
    });
    print(listCart.length.toString() + " ssa");
  }

  // void countTotal() {
  //   total.value =0;
  //   if(listCart.length > 0){
  //     listCart.forEach((element) {
  //       total.value += (element.price! *element.qty!);
  //       update();
  //     });
  //     update();
  //   }else{
  //     total.value = 0;
  //     update();
  //   }
  // }

  int desc(CartModel cartModel) {
    cartModel.qty = cartModel.qty! + 1;
    total.value += cartModel.price!;
    update();
    listCart.refresh();
    return cartModel.qty!;
  }

  int asc(CartModel cartModel) {
    cartModel.qty = cartModel.qty! - 1;
    total.value -= cartModel.price!;
    update();
    listCart.refresh();
    return cartModel.qty!;
  }
}
