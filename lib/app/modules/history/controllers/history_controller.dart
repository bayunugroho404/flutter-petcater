import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:petcater/app/data/remote/api/api_config.dart';
import 'package:petcater/app/data/remote/responses/response_get_transaction.dart';
import 'package:petcater/app/model.dart';
import 'package:petcater/utils/secure_storage.dart';

class HistoryController extends GetxController {
  final listHistory = <Datum>[].obs;
  final format = new DateFormat('dd-MMM-yyyy hh:mm');
  final secure = SecureStorage();

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void addHistory() async{
    String? token = await secure.getToken();
    listHistory.clear();
    APIService().getTransaction(token!).then((value){
      listHistory.value = value.data!;
      update();
    }).catchError((_){});
    update();
  }
  String formatDate(DateTime date){
    String dateFormat = format.format(date);
    return dateFormat.toString();
  }
}
