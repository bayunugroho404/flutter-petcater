import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:petcater/app/data/remote/responses/response_get_transaction.dart';
import 'package:petcater/utils/constant.dart';
import 'package:petcater/utils/size_config.dart';

import '../controllers/history_controller.dart';

class HistoryView extends GetView<HistoryController> {
  final controller = Get.put(HistoryController());

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    controller.addHistory();
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios,color: primary,),
            onPressed: (){Get.back();},
          ),
          title: Text(
            'History',
            textAlign: TextAlign.left,
            style: GoogleFonts.lato(
              color: grey,
              fontSize: SizeConfig.blockVertical * 3,
            ),
          ),
        ),
        body: Obx(() => controller.listHistory.length > 0
            ? Container(
          width: SizeConfig.screenWidth,
          child: Expanded(
            child: ListView.builder(
              shrinkWrap: true,
              primary: false,
              scrollDirection: Axis.vertical,
              itemCount: controller.listHistory.length,
              itemBuilder: (
                  context,
                  index,
                  ) {
                return
                  ExpansionTile(
                    title: Row(
                      children: [
                        Text(
                          "${controller.listHistory[index].code} || ${controller.formatDate(controller.listHistory[index].updatedAt!)}",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    subtitle: Row(
                      children: [
                        Text(
                          "${setStatus(controller.listHistory[index])}",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Rp.${controller.listHistory[index].totalPrice}'),
                      ),
                    ],
                  );
              },
            ),
          ),
        )
            : Center(child: CircularProgressIndicator())));
  }

  String setStatus(Datum listHistory) {
    if(listHistory.transactionStatus == "PENDING"){
      return "Parcel sedang diproses";
    }else if(listHistory.transactionStatus == "SHIPPING"){
      return "Parcel sedang dikirim";
    }else{
      return "Parcel telah sampai tujuan";
    }
  }
}
