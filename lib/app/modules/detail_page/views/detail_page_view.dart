import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:petcater/app/data/remote/responses/response_product.dart';
import 'package:petcater/app/data/remote/responses/product_by_category.dart'
    as pc;
import 'package:petcater/utils/constant.dart';
import 'package:petcater/utils/size_config.dart';

import '../controllers/detail_page_controller.dart';

class DetailPageView extends GetView<DetailPageController> {
  String? source;
  String? name;
  String? image;
  String? price;
  List<Gallery>? imageGallery = [];
  List<pc.Gallery>? imageGallery2 = [];
  String? desc;
  String? id;

  DetailPageView(
      {this.name,
      this.price,
      this.image,
      this.desc,
      this.id,
      this.imageGallery,
      this.imageGallery2,
      this.source});

  final controller = Get.put(DetailPageController());

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primary,
        title: Text('$name'),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: primary,
        child: Icon(
          Icons.add_shopping_cart_outlined,
        ),
        onPressed: () {
          AwesomeDialog(
            context: context,
            animType: AnimType.SCALE,
            dialogType: DialogType.INFO,
            body: Center(
              child: TextField(
                controller: controller.kuantitasController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "kuantitas",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0))),
              ),
            ),
            title: 'This is Ignored',
            btnOkText: "Submit",
            btnOkColor: primary,
            desc: 'This is also Ignored',
            btnOkOnPress: () {
              controller.addCart(
                  controller.kuantitasController.text, id!, context);
            },
          )..show();
        },
      ),
      body: Container(
        child: Column(
          children: [
            //detail from category
            source == "category"
                ? imageGallery2!.length > 0
                    ? Container(
                        height: SizeConfig.screenHight / 3,
                        child: CarouselSlider(
                          options: CarouselOptions(
                            aspectRatio: 2.0,
                            autoPlayCurve: Curves.easeInQuint,
                            enlargeCenterPage: false,
                            enableInfiniteScroll: true,
                            initialPage: 2,
                            onPageChanged: (index, reason) {},
                            autoPlay: true,
                          ),
                          items: imageGallery2!
                              .map((item) => Container(
                                    child: Center(
                                        child: Image.network(item.photos!,
                                            fit: BoxFit.cover, width: 1000)),
                                  ))
                              .toList(),
                        ),
                      )
                //detail from dashboard
                : Image.network(
                        image!,
                        width: SizeConfig.screenWidth,
                        height: SizeConfig.screenWidth / 2.5,
                        fit: BoxFit.fill,
                      )
                : imageGallery!.length > 0
                    ? Container(
                        height: SizeConfig.screenHight / 3,
                        child: CarouselSlider(
                          options: CarouselOptions(
                            aspectRatio: 2.0,
                            autoPlayCurve: Curves.easeInQuint,
                            enlargeCenterPage: false,
                            enableInfiniteScroll: true,
                            initialPage: 2,
                            onPageChanged: (index, reason) {},
                            autoPlay: true,
                          ),
                          items: imageGallery!
                              .map((item) => Container(
                                    child: Center(
                                        child: Image.network(item.photos!,
                                            fit: BoxFit.cover, width: 1000)),
                                  ))
                              .toList(),
                        ),
                      )
                    : Image.network(
                        image!,
                        width: SizeConfig.screenWidth,
                        height: SizeConfig.screenWidth / 2.5,
                        fit: BoxFit.fill,
                      ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Divider(),
            ),
            Container(
              width: SizeConfig.screenWidth,
              child: Padding(
                padding: EdgeInsets.only(left: 20, top: 20, bottom: 4),
                child: Text(
                  '$name, \nRp. $price',
                  textAlign: TextAlign.left,
                  style: GoogleFonts.lato(
                    fontSize: SizeConfig.blockVertical * 3,
                  ),
                ),
              ),
            ),
            Container(
              width: SizeConfig.screenWidth,
              child: Padding(
                padding: EdgeInsets.only(left: 20, top: 20, bottom: 4),
                child: Html(
                  data: "$desc",
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
