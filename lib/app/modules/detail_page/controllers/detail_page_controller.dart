import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:petcater/app/data/remote/api/api_config.dart';
import 'package:petcater/app/modules/home/views/home_view.dart';
import 'package:petcater/app/modules/widgets/toast.dart';
import 'package:petcater/utils/secure_storage.dart';

class DetailPageController extends GetxController {
  final secure = SecureStorage();
  TextEditingController kuantitasController = TextEditingController();


  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void addCart(String qty,String productId,BuildContext context)async{
    bool token = await secure.hasToken();
    if(token){
      String? token = await secure.getToken();
      if(qty.isEmpty){
        showToast("qty tidak boleh kosong");
      }else{
        APIService().postCart(token!, productId, qty).then((val){
          if(val.success == true){
            showToast("berhasil ditambah ke keranjang");
            Get.offAll(HomeView());

            // Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => HomeView()));
          }else{
            showToast("gagal ditambah ke keranjang");
          }
        }).catchError((e){
          showToast("gagal ditambah ke keranjang");
        });
      }
    }
  }
}
