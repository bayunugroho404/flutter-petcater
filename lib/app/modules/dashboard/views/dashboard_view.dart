import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:petcater/app/modules/category/views/category_view.dart';
import 'package:petcater/app/modules/detail_page/views/detail_page_view.dart';
import 'package:petcater/utils/constant.dart';
import 'package:petcater/utils/size_config.dart';

import '../controllers/dashboard_controller.dart';

class DashboardView extends GetView<DashboardController> {
  final controller = Get.put(DashboardController());

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Container(
          width: SizeConfig.screenWidth,
          margin: EdgeInsets.only(top: 50),
          child: ListView(
            primary: true,
            children: [
              CarouselSlider(
                options: CarouselOptions(
                  aspectRatio: 2.0,
                  enlargeCenterPage: true,
                  enableInfiniteScroll: true,
                  initialPage: 2,
                  onPageChanged: (index, reason) {
                    controller.updateIndex(index);
                  },
                  autoPlay: true,
                ),
                items: controller.imgList
                    .map((item) => Container(
                          child: Center(
                              child: Image.network(item,
                                  fit: BoxFit.cover, width: 1000)),
                        ))
                    .toList(),
              ),
              Container(
                width: SizeConfig.screenWidth,
                child: Padding(
                  padding: EdgeInsets.only(left: 20, top: 20, bottom: 4),
                  child: Text(
                    'Category',
                    textAlign: TextAlign.left,
                    style: GoogleFonts.lato(
                      fontSize: SizeConfig.blockVertical * 3,
                    ),
                  ),
                ),
              ),
              Divider(),
              Obx(() => controller.listCategory.length > 0
                  ? Container(
                      width: SizeConfig.screenWidth,
                      height: 100,
                      child: ListView.builder(
                        shrinkWrap: true,
                        primary: false,
                        scrollDirection: Axis.horizontal,
                        itemCount: controller.listCategory.length,
                        itemBuilder: (
                          context,
                          index,
                        ) {
                          return InkWell(
                            onTap: () {
                              Get.to(CategoryView(
                                name: controller.listCategory[index].name,
                                id: controller.listCategory[index].id,
                              ));
                            },
                            child: Card(
                              child: Container(
                                width: SizeConfig.screenWidth / 3,
                                margin: EdgeInsets.all(8.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Image.network(
                                      controller.listCategory[index].image!,
                                      width: 50,
                                      height: 50,
                                    ),
                                    SizedBox(
                                      height: 4,
                                    ),
                                    Text(
                                        '"${controller.listCategory[index].name}"'),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    )
                  : Container(
                  width: SizeConfig.screenWidth,
                  height: SizeConfig.screenHight,
                  child: Center(child: CircularProgressIndicator()))),
              Container(
                width: SizeConfig.screenWidth,
                child: Padding(
                  padding: EdgeInsets.only(left: 20, top: 20, bottom: 4),
                  child: Text(
                    'Produk Terbaru',
                    textAlign: TextAlign.left,
                    style: GoogleFonts.lato(
                      fontSize: SizeConfig.blockVertical * 3,
                    ),
                  ),
                ),
              ),
              Divider(),
              Obx(() => controller.listProduct.length > 0
                  ? Container(
                      margin: EdgeInsets.only(left: 10, right: 10),
                      width: SizeConfig.screenWidth,
                      child: GridView.builder(
                        shrinkWrap: true,
                        primary: false,
                        itemCount: controller.listProduct.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          mainAxisSpacing: 3.0,
                          childAspectRatio: 1,
                        ),
                        itemBuilder: (
                          context,
                          index,
                        ) {
                          return Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            elevation: 4,
                            child: GestureDetector(
                              onTap: () {
                                Get.to(DetailPageView(
                                    name: controller.listProduct[index].name!,
                                    price: controller.listProduct[index].price!,
                                    id: controller.listProduct[index].id
                                        .toString(),
                                    desc: controller.listProduct[index].desc,
                                    image: controller.listProduct[index].image!,
                                    imageGallery: controller
                                        .listProduct[index].imageGallery));
                              },
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Flexible(
                                    flex: 2,
                                    child: Container(
                                      decoration: BoxDecoration(
                                          border:
                                              Border.all(color: Colors.white)),
                                      child: Image.network(
                                        (controller.listProduct[index].imageGallery!.length>0?
                                        controller.listProduct[index].imageGallery![0].photos:
                                        controller.listProduct[index].image!)!,
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                  ),
                                  Flexible(
                                    flex: 1,
                                    child: Container(
                                      width: SizeConfig.screenWidth,
                                      child: Text(
                                        controller.listProduct[index].name! +
                                            "\n" +
                                            "Rp." +
                                            controller
                                                .listProduct[index].price!,
                                        textAlign: TextAlign.center,
                                        style: GoogleFonts.lato(
                                          fontSize:
                                              SizeConfig.blockVertical * 2,
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    )
                  : Container(
                  width: SizeConfig.screenWidth,
                  height: SizeConfig.screenHight,
                  child: Center(child: CircularProgressIndicator()))),
            ],
          )),
    );
  }
}
