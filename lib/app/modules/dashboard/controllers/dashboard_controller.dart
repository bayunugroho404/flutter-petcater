import 'package:get/get.dart';
import 'package:petcater/app/data/remote/api/api_config.dart';
import 'package:petcater/app/model.dart';

class DashboardController extends GetxController {
  final listCategory = <CategoryModel>[].obs;
  final listProduct = <ProductModel>[].obs;
  final imgList = <String>[
    'https://static.republika.co.id/uploads/images/inpicture_slide/bentuk-pelayanan-di-pet_201121072907-515.jpg',
    'https://jogjaasik.com/wp-content/uploads/2020/08/Rekomendasi-Petshop-Terlengkap-di-Jogja.jpg',
    'https://2.bp.blogspot.com/-Z66IQbCbXXg/XIvrO-FDYdI/AAAAAAAAuZo/UzBerXjD9ms_zWd4X8ZY2U7y9YOZgjaTQCLcBGAs/s1600/Club%2BPet%2BTaman%2BPramuka%2BBandung.jpg',
    'https://indolinear.com/wp-content/uploads/2018/11/petshop.jpg'
  ].obs;
  final current = 0.obs;
  final index = 0.obs;


  @override
  void onInit() {
    addCategory();
    addProduct();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void addCategory() {
    listCategory.clear();
    APIService().getCategory().then((val){
      if(val.data!.length>0){
        val.data!.forEach((element) {
          print(element.photo);
          listCategory.add(new CategoryModel(
              name: element.name,
              id: element.id,
              // image: "https://pbs.twimg.com/media/EUU6kHiUMAAcOZs.png"
              image: element.photo
          ));
        });
        update();
      }
    });
    update();
  }

  void addProduct() {
    listProduct.clear();
    APIService().getProduct().then((val){
      if(val.data!.length >0){
        val.data!.forEach((element) {
          listProduct.add(new ProductModel(
              name: element.name,
              id: element.id,
              desc: element.description,
              price: element.price.toString(),
              image: "https://images.tokopedia.net/img/cache/215-square/shops-1/2020/12/20/9492632/9492632_eea5fd7a-e971-4018-9302-9ca9383ee3d2.jpg",
              imageGallery: element.galleries!
          ));
        });
        update();
      }
    });

  }

  void updateCarousel( String url){
    index.value = imgList.indexOf(url);
    update;
  }

  void updateIndex(int index){
    current.value = index;
    update();
  }
}
