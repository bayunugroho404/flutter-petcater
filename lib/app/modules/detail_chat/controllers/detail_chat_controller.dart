import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:petcater/app/data/remote/api/api_config.dart';
import 'package:petcater/app/model.dart';
import 'package:petcater/utils/secure_storage.dart';

class DetailChatController extends GetxController {
  final listCart = <DetailChatModel>[].obs;
  final ScrollController listScrollController = ScrollController();
  final limit = 20.obs;
  TextEditingController msgController= new TextEditingController();
  final limitIncrement = 20.obs;
  final count = 0.obs;
  PickedFile? imageFile;
  dynamic pickImageError;
  final _picker = ImagePicker();

  final secure = SecureStorage();



  @override
  void onInit() {
    listScrollController.addListener(scrollListener);
    super.onInit();
  }


  scrollListener() {
    if (listScrollController.offset >= listScrollController.position.maxScrollExtent &&
        !listScrollController.position.outOfRange) {
        limit.value += limitIncrement.value;
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  Future getImageFromGallery() async {
    try {
      final pickedFile = await _picker.getImage(
        source: ImageSource.gallery,
      );
      imageFile = pickedFile;
      print(imageFile?.path);
    } catch (e) {
      pickImageError = e;
    }
    update();
  }
  void addChat(String receiver) async{
    String? token = await secure.getToken();
    String? idUser = await secure.getIdUser();
    listCart.clear();

    await APIService().getAllChat(token!, idUser!,receiver).then((value) {
      value.data!.forEach((element) {
        print("os musuh " + element.receiver!.os_id.toString());
        listCart.add(new DetailChatModel(
            id: element.senderId.toString(),
            name: element.message,
            image: element.image,
            time: element.createdAt,
            os_id: element.receiver?.os_id
        ));
      });
      update();
    }).catchError((val){
    });

    await APIService().getAllChat(token,receiver,idUser).then((value) {
      value.data!.forEach((element) {
        listCart.add(new DetailChatModel(
            id: element.senderId.toString(),
            name: element.message,
            image: element.image,
            time: element.createdAt
        ));
      });
      update();
    }).catchError((val){
    });
    listCart.sort((a,b) => a.time!.millisecondsSinceEpoch.compareTo(b.time!.millisecondsSinceEpoch));
    update();
  }

  void sendMessage(String receiver_id,String message, String? name, String? os_id)async{
    String? token = await secure.getToken();
    String? idUser = await secure.getIdUser();
    String? sender_name = await secure.getName();
    APIService().sendChat(token!, idUser!, receiver_id, message).then((val){
      if(val.success ==true){
        APIService().sendNotif(
            """ {
              "to" : "$os_id",
              "collapse_key" : "New Message",
              "priority": "high",
              "click_action": "FLUTTER_NOTIFICATION_CLICK",
              "notification" : {
                "title": "$sender_name",
                "body" : "${msgController.text}",
                "sound": "emergency.mp3"
              }
            } """
        ).then((value){
          addChat(receiver_id);
          msgController.text = '';
        }).catchError((onError){
          print(onError + " errorku");
        });
      }
    }).catchError((onError){

    });
  }


  Future<void> requestPermission(Permission permission) async {
    await permission.request();
  }
}
