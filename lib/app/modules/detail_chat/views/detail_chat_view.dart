import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:petcater/app/modules/chat_image/views/chat_image_view.dart';
import 'package:petcater/app/modules/widgets/toast.dart';
import 'package:petcater/utils/constant.dart';
import 'package:petcater/utils/size_config.dart';

import '../controllers/detail_chat_controller.dart';

class DetailChatView extends GetView<DetailChatController> {
  String? name;
  String? os_id;
  String? id;
  Future<bool>? refresh;

  final controller = Get.put(DetailChatController());

  DetailChatView({this.name,this.id,this.refresh,this.os_id});

  @override
  Widget build(BuildContext context) {
    controller.addChat(id!);
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
           Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back_ios,color: primary),
        ),
        backgroundColor: Colors.white,
        title: Text(
          '$name',
          textAlign: TextAlign.left,
          style: GoogleFonts.lato(
            color: grey,
            fontSize: SizeConfig.blockVertical * 3,
          ),
        ),
      ),
      body:Stack(
        children: [
          Column(children: [
            buildListMessage(),
            Container(),
            typeMessage(context)
          ],)
        ],
      ),
    );
  }

  Widget buildListMessage(){
    return Flexible(
      child: Obx(() => controller.listCart.length > 0
          ? ListView.builder(
            controller: controller.listScrollController,
            scrollDirection: Axis.vertical,
            padding: EdgeInsets.all(10.0),
            itemCount: controller.listCart.length,
            itemBuilder: (
                context,
                index,
                ) {
              return controller.listCart[index].id == id?
              Row(
                children: [
                  Container(
                    width: SizeConfig.screenWidth * 0.9,
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: controller.listCart[index].name != null?
                      //text left
                      Container(
                          padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                          width: 100,
                          margin: EdgeInsets.only(bottom: 10.0, right: 10.0),
                          decoration: BoxDecoration(color: Color.fromRGBO(225, 255, 199, 1.0), borderRadius: BorderRadius.circular(8.0)),
                          child:Text("${controller.listCart[index].name}")):
                      //image left
                      Container(
                          padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                          width: 200,
                          margin: EdgeInsets.only(bottom: 10.0, right: 10.0),
                          decoration: BoxDecoration(color: Color.fromRGBO(225, 255, 199, 1.0), borderRadius: BorderRadius.circular(8.0)),
                          child:Image.network("${controller.listCart[index].image}",fit: BoxFit.contain,)),
                    ),
                  )
                ],
              ):Row(
                children: [
                  Container(
                    width: SizeConfig.screenWidth * 0.9,
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: controller.listCart[index].name != null?
                      //text right
                      Container(
                          padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                          width: 100,
                          margin: EdgeInsets.only(bottom: 10.0, right: 10.0),
                          decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(8.0)),
                          child:Text("${controller.listCart[index].name}")):
                      //image right
                      Container(
                          padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                          width: 200,
                          margin: EdgeInsets.only(bottom: 10.0, right: 10.0),
                          decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(8.0)),
                          child:Image.network("${controller.listCart[index].image}",fit: BoxFit.contain,)),
                    ),
                  )
                ],
              );
              },
          )
          : SizedBox(height: SizeConfig.screenHight,)),
    );
  }
  Widget typeMessage(BuildContext context){
    return Container(
      child: Row(
        children: <Widget>[
          // Button send image
          Material(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 1.0),
              child: IconButton(
                icon: Icon(Icons.image),
                onPressed: ()async{
                  controller.requestPermission(Permission.storage);
                  if (await Permission.storage.request().isGranted) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ChatImageView(
                              receiver_id:id,
                            ))).then((value) {
                      controller.addChat(id!);
                    });
                  }
                },
                color: primary,
              ),
            ),
            color: Colors.white,
          ),
          Material(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 1.0),
              child: IconButton(
                icon: Icon(Icons.face),
                onPressed: null,
                color: primary,
              ),
            ),
            color: Colors.white,
          ),

          // Edit text
          Flexible(
            child: Container(
              child: TextField(
                style: TextStyle(color: primary, fontSize: 15.0),
                controller: controller.msgController,
                decoration: InputDecoration.collapsed(
                  hintText: 'Type your message...',
                  hintStyle: TextStyle(color: grey),
                ),
              ),
            ),
          ),

          // Button send message
          Material(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 8.0),
              child: IconButton(
                icon: Icon(Icons.send),
                onPressed: (){
                  if(controller.msgController.text.trim().length <1){
                    showToast("pesan tidak boleh kosong");
                  }else{
                    controller.sendMessage(id!, controller.msgController.text,name,os_id);
                  }
                },
                color: primary,
              ),
            ),
            color: Colors.white,
          ),
        ],
      ),
      width: double.infinity,
      height: 50.0,
      decoration: BoxDecoration(border: Border(top: BorderSide(color: Colors.grey, width: 0.5)), color: Colors.white),
    );
  }
}
