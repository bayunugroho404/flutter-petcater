import 'package:get/get.dart';
import 'package:petcater/app/data/remote/api/api_config.dart';
import 'package:petcater/app/modules/widgets/toast.dart';

class SignupController extends GetxController {
  //TODO: Implement SignupController

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void postRegister(String name,String email,String password,String password_confirmation, String address,){
    APIService().postRegister(name, email, password, password_confirmation,address).then((val){
      if(val.token!.length > 2){
        Get.back();
        showToast("berhasil register");
        print('dadas');
      }else{
        showToast("gagal");
        print('dsadsaj');
      }
    }).catchError((onError){
      print('$onError');
      showToast("$onError");
    });
  }

}
