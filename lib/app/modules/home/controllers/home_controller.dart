import 'package:get/get.dart';
import 'package:petcater/app/modules/cart/views/cart_view.dart';
import 'package:petcater/app/modules/chat/views/chat_view.dart';
import 'package:petcater/app/modules/dashboard/views/dashboard_view.dart';
import 'package:petcater/app/modules/profile/views/profile_view.dart';
import 'package:petcater/app/modules/signin/views/signin_view.dart';
import 'package:petcater/app/modules/signup/views/signup_view.dart';

class HomeController extends GetxController {
  final selectedIndex = 0.obs;
  final tabs = [
    DashboardView(),
    ChatView(),
    CartView(),
    ProfileView()
  ];

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void updateTabSelection(int index, String buttonText) {
    selectedIndex.value = index;
  }
}
