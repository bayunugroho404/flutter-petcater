import 'dart:io';

import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:petcater/utils/constant.dart';

import '../controllers/chat_image_controller.dart';

class ChatImageView extends GetView<ChatImageController> {
  String? receiver_id;

  ChatImageView({this.receiver_id});
  final controller = Get.put(ChatImageController());


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('upload image'),
        backgroundColor: primary,
        centerTitle: true,
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Card(
                clipBehavior: Clip.antiAlias,
                elevation: 0.0,
                color: Colors.grey.shade300,
                child: GetBuilder<ChatImageController>(
                  builder: (_){
                    return Column(
                      children: [
                        controller.imageFile != null ?Semantics(
                            child: Image.file(File(controller.imageFile!.path)),
                            label: 'image_picker_example_picked_image'):Text('Image not selected'),
                        ListTile(
                          title: Padding(
                            padding: EdgeInsets.only(bottom: 00.0, top: 00.0),
                            child: new RaisedButton(
                              child: Text(
                                  "Choose Image"),
                              color: primary,
                              textColor: Colors.white,
                              onPressed: () {
                                controller.getImageFromGallery();
                              },
                            ),
                          ),
                        ),
                        Visibility(
                          visible: controller.imageFile != null?true:false,
                          child: ListTile(
                            title: Padding(
                              padding: EdgeInsets.only(bottom: 00.0, top: 00.0),
                              child: new RaisedButton(
                                child: Text(
                                    "Upload"),
                                color: primary,
                                textColor: Colors.white,
                                onPressed: () {
                                  controller.postChat(controller.imageFile!, receiver_id!, context);
                                },
                              ),
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
