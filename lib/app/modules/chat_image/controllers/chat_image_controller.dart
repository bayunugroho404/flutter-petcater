import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:petcater/app/data/remote/api/api_config_image.dart';
import 'package:petcater/utils/secure_storage.dart';

class ChatImageController extends GetxController {
  PickedFile? imageFile;
  final storage = new SecureStorage();
  dynamic pickImageError;
  final _picker = ImagePicker();

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  Future getImageFromGallery() async {
    try {
      final pickedFile = await _picker.getImage(
        source: ImageSource.gallery,
      );
      imageFile = pickedFile;
      print(imageFile?.path);
    } catch (e) {
      pickImageError = e;
    }
    update();
  }

  void postChat(PickedFile image, String receiver_id, BuildContext context)async{
    String token = (await storage.getToken())!;
    String? idUser = await storage.getIdUser();
    ApiServiceImage().postChat(token, image, idUser!, receiver_id).then((data){
      if(data.success == true){
        Navigator.pop(context);
      }else{
        print(data.message);
      }
    }).catchError((onError){
      print("error" + onError.toString());
    });
  }

}
