import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:petcater/utils/constant.dart';
import 'package:petcater/utils/size_config.dart';

import '../controllers/detail_laporan_controller.dart';

class DetailLaporanView extends GetView<DetailLaporanController> {
  String? id;
  String? code;
  DetailLaporanView({this.id,this.code});
  final controller = Get.put(DetailLaporanController());

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    controller.getLaporan(id!);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primary,
        title: Text('$code'),
        centerTitle: true,
      ),
      body: Obx(() =>
      controller.listLaporan.length > 0
          ? Container(
        width: SizeConfig.screenWidth,
        height: SizeConfig.screenHight /1.4,
        child: ListView.builder(
          shrinkWrap: true,
          primary: false,
          scrollDirection: Axis.vertical,
          itemCount: controller.listLaporan.length,
          itemBuilder: (context,
              index,) {
            return Column(
              children: [
                ListTile(
                  // title: Text(controller.listLaporan[index].product!.name! +'(${controller.listLaporan[index].product!.qty})'),
                  title: Text(controller.listLaporan[index].product!.name!),
                  subtitle: Text('Rp.'+controller.listLaporan[index].product!.price.toString()),
                ),
                Divider()
              ],
            );
            ;
          },
        ),
      )
          : Center(child: Text('tidak ada produk di keranjang'))),
    );
  }
}
