import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:petcater/app/modules/home/views/home_view.dart';
import 'package:petcater/app/modules/signup/views/signup_view.dart';
import 'package:petcater/utils/constant.dart';
import 'package:petcater/utils/size_config.dart';

import '../controllers/signin_controller.dart';

class SigninView extends GetView<SigninController> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final controller = Get.put(SigninController());

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Container(
        height: SizeConfig.screenHight,
        width: SizeConfig.screenWidth,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: SizeConfig.screenHight / 8,),
                Image.asset("assets/zzz.png"),
                TextField(
                  controller: emailController,
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      hintText: "Email",
                      border:
                      OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                ),
                SizedBox(height: 25.0),
                TextField(
                  obscureText: true,
                  controller: passwordController,
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      hintText: "Password",
                      border:
                      OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                ),
                SizedBox(height: 25.0),
                Material(
                  elevation: 5.0,
                  borderRadius: BorderRadius.circular(30.0),
                  color: primary,
                  child: MaterialButton(
                    minWidth: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    onPressed: () {
                      controller.postLogin(emailController.text, passwordController.text);
                    },
                    child: Text("Login",
                      textAlign: TextAlign.center,),
                  ),
                ),
                SizedBox(height: 20.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text('Belum punya akun ? '),
                    InkWell(
                        onTap: (){
                          Get.to(SignupView());
                          },
                        child: Text('Daftar',style: TextStyle(color: Colors.blue),)),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

}
