import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:petcater/app/data/remote/api/api_config.dart';
import 'package:petcater/app/data/remote/responses/response_login.dart';
import 'package:petcater/app/modules/home/views/home_view.dart';
import 'package:petcater/app/modules/widgets/toast.dart';
import 'package:petcater/utils/secure_storage.dart';

import '../../../../main.dart';

class SigninController extends GetxController {
  //TODO: Implement SigninController
  final secure = SecureStorage();

  final count = 0.obs;
  final os_id = ''.obs;

  @override
  void onInit() {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channel.description,
                color: Colors.blue,
                playSound: true,
                icon: '@mipmap/ic_launcher',
              ),
            ));
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('A new onMessageOpenedApp event was published!');
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      if (notification != null && android != null) {
        Get.dialog(AlertDialog(
          title: Text(notification.title!),
          content: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [Text(notification.body!)],
            ),
          ),
        ));
      }
    });
    checkUser();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void increment() => count.value++;

  void postLogin(String email, String password) {
     fcm(email,password);
  }

  void savetoLocal(String token, Data? data) {
    secure.persistUser(token, data!.email!, data.id.toString(), data.roles!,
        data.name!, data.addressOne!.toString());
    Get.offAll(HomeView());
    showToast("berhasil login");
  }

  void checkUser() async {
    bool role = await secure.hasToken();
    if (role) {
      Get.offAll(HomeView());
    }
  }

  void fcm(String email,String password) async{
    FirebaseMessaging messaging = FirebaseMessaging.instance;
    os_id.value = (await messaging.getToken())!;
    update();

    APIService().postLogin(email, password,os_id.value).then((val) {
        savetoLocal(val.token!, val.data);

    }).catchError((onError) {
      showToast("$onError");
    });
  }
}
