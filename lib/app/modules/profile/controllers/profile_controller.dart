import 'package:get/get.dart';
import 'package:petcater/app/data/remote/api/api_config.dart';
import 'package:petcater/app/modules/signin/views/signin_view.dart';
import 'package:petcater/app/modules/widgets/toast.dart';
import 'package:petcater/utils/secure_storage.dart';

class ProfileController extends GetxController {
  final secure = SecureStorage();
  final name = ''.obs;
  final role = ''.obs;
  final email = ''.obs;
  final address = ''.obs;
  final phoneNumber = ''.obs;

  final count = 0.obs;

  @override
  void onInit() {
    getUser();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;


  void doLogout(){
    secure.deleteAll();
    Get.offAll(SigninView());
  }

  void getUser() async{
    email.value = (await secure.getEmail())!;
    name.value = (await secure.getName())!;
    role.value = (await secure.getRole())!;
    address.value = (await secure.getAddress())!;
    update();
  }

  void updateProfile(String name, String email, String address) async{
    String token = (await secure.getToken())!;

    APIService().updateProfile(token,name,email,address).then((val){
      if(val.success == true){
        showToast("Profile update Successfull");
        secure.updateUser(email,name,address);
        getUser();
      }
    });
  }

}
