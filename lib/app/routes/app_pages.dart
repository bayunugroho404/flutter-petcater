import 'package:get/get.dart';

import 'package:petcater/app/modules/cart/bindings/cart_binding.dart';
import 'package:petcater/app/modules/cart/views/cart_view.dart';
import 'package:petcater/app/modules/category/bindings/category_binding.dart';
import 'package:petcater/app/modules/category/views/category_view.dart';
import 'package:petcater/app/modules/chat/bindings/chat_binding.dart';
import 'package:petcater/app/modules/chat/views/chat_view.dart';
import 'package:petcater/app/modules/chat_image/bindings/chat_image_binding.dart';
import 'package:petcater/app/modules/chat_image/views/chat_image_view.dart';
import 'package:petcater/app/modules/dashboard/bindings/dashboard_binding.dart';
import 'package:petcater/app/modules/dashboard/views/dashboard_view.dart';
import 'package:petcater/app/modules/detail_chat/bindings/detail_chat_binding.dart';
import 'package:petcater/app/modules/detail_chat/views/detail_chat_view.dart';
import 'package:petcater/app/modules/detail_laporan/bindings/detail_laporan_binding.dart';
import 'package:petcater/app/modules/detail_laporan/views/detail_laporan_view.dart';
import 'package:petcater/app/modules/detail_page/bindings/detail_page_binding.dart';
import 'package:petcater/app/modules/detail_page/views/detail_page_view.dart';
import 'package:petcater/app/modules/history/bindings/history_binding.dart';
import 'package:petcater/app/modules/history/views/history_view.dart';
import 'package:petcater/app/modules/home/bindings/home_binding.dart';
import 'package:petcater/app/modules/home/views/home_view.dart';
import 'package:petcater/app/modules/laporan/bindings/laporan_binding.dart';
import 'package:petcater/app/modules/laporan/views/laporan_view.dart';
import 'package:petcater/app/modules/order/bindings/order_binding.dart';
import 'package:petcater/app/modules/order/views/order_view.dart';
import 'package:petcater/app/modules/profile/bindings/profile_binding.dart';
import 'package:petcater/app/modules/profile/views/profile_view.dart';
import 'package:petcater/app/modules/signin/bindings/signin_binding.dart';
import 'package:petcater/app/modules/signin/views/signin_view.dart';
import 'package:petcater/app/modules/signup/bindings/signup_binding.dart';
import 'package:petcater/app/modules/signup/views/signup_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.SIGNIN;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.SIGNIN,
      page: () => SigninView(),
      binding: SigninBinding(),
    ),
    GetPage(
      name: _Paths.SIGNUP,
      page: () => SignupView(),
      binding: SignupBinding(),
    ),
    GetPage(
      name: _Paths.DASHBOARD,
      page: () => DashboardView(),
      binding: DashboardBinding(),
    ),
    GetPage(
      name: _Paths.CATEGORY,
      page: () => CategoryView(),
      binding: CategoryBinding(),
    ),
    GetPage(
      name: _Paths.CART,
      page: () => CartView(),
      binding: CartBinding(),
    ),
    GetPage(
      name: _Paths.CHAT,
      page: () => ChatView(),
      binding: ChatBinding(),
    ),
    GetPage(
      name: _Paths.PROFILE,
      page: () => ProfileView(),
      binding: ProfileBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_PAGE,
      page: () => DetailPageView(),
      binding: DetailPageBinding(),
    ),
    GetPage(
      name: _Paths.ORDER,
      page: () => OrderView(),
      binding: OrderBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_CHAT,
      page: () => DetailChatView(),
      binding: DetailChatBinding(),
    ),
    GetPage(
      name: _Paths.HISTORY,
      page: () => HistoryView(),
      binding: HistoryBinding(),
    ),
    GetPage(
      name: _Paths.LAPORAN,
      page: () => LaporanView(),
      binding: LaporanBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_LAPORAN,
      page: () => DetailLaporanView(),
      binding: DetailLaporanBinding(),
    ),
    GetPage(
      name: _Paths.CHAT_IMAGE,
      page: () => ChatImageView(),
      binding: ChatImageBinding(),
    ),
  ];
}
