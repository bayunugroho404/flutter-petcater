part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();

  static const HOME = _Paths.HOME;
  static const SIGNIN = _Paths.SIGNIN;
  static const SIGNUP = _Paths.SIGNUP;
  static const DASHBOARD = _Paths.DASHBOARD;
  static const CATEGORY = _Paths.CATEGORY;
  static const CART = _Paths.CART;
  static const CHAT = _Paths.CHAT;
  static const PROFILE = _Paths.PROFILE;
  static const DETAIL_PAGE = _Paths.DETAIL_PAGE;
  static const ORDER = _Paths.ORDER;
  static const DETAIL_CHAT = _Paths.DETAIL_CHAT;
  static const HISTORY = _Paths.HISTORY;
  static const LAPORAN = _Paths.LAPORAN;
  static const DETAIL_LAPORAN = _Paths.DETAIL_LAPORAN;
  static const CHAT_IMAGE = _Paths.CHAT_IMAGE;
}

abstract class _Paths {
  static const HOME = '/home';
  static const SIGNIN = '/signin';
  static const SIGNUP = '/signup';
  static const DASHBOARD = '/dashboard';
  static const CATEGORY = '/category';
  static const CART = '/cart';
  static const CHAT = '/chat';
  static const PROFILE = '/profile';
  static const DETAIL_PAGE = '/detail-page';
  static const ORDER = '/order';
  static const DETAIL_CHAT = '/detail-chat';
  static const HISTORY = '/history';
  static const LAPORAN = '/laporan';
  static const DETAIL_LAPORAN = '/detail-laporan';
  static const CHAT_IMAGE = '/chat-image';
}
