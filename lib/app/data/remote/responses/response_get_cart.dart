// To parse this JSON data, do
//
//     final responseGetCart = responseGetCartFromJson(jsonString);

import 'dart:convert';

ResponseGetCart responseGetCartFromJson(String str) => ResponseGetCart.fromJson(json.decode(str));

String responseGetCartToJson(ResponseGetCart data) => json.encode(data.toJson());

class ResponseGetCart {
  ResponseGetCart({
    this.success,
    this.data,
    this.message,
  });

  bool? success;
  List<Datum>? data;
  String? message;

  factory ResponseGetCart.fromJson(Map<String, dynamic> json) => ResponseGetCart(
    success: json["success"] == null ? null : json["success"],
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "success": success == null ? null : success,
    "data": data == null ? null : List<dynamic>.from(data!.map((x) => x.toJson())),
    "message": message == null ? null : message,
  };
}

class Datum {
  Datum({
    this.id,
    this.usersId,
    this.productsId,
    this.createdAt,
    this.updatedAt,
    this.qty,
    this.product,
    this.user,
  });

  int? id;
  int? usersId;
  int? productsId;
  DateTime? createdAt;
  DateTime? updatedAt;
  int? qty;
  Product? product;
  User? user;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"] == null ? null : json["id"],
    usersId: json["users_id"] == null ? null : json["users_id"],
    productsId: json["products_id"] == null ? null : json["products_id"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    qty: json["qty"] == null ? null : json["qty"],
    product: json["product"] == null ? null : Product.fromJson(json["product"]),
    user: json["user"] == null ? null : User.fromJson(json["user"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "users_id": usersId == null ? null : usersId,
    "products_id": productsId == null ? null : productsId,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
    "qty": qty == null ? null : qty,
    "product": product == null ? null : product?.toJson(),
    "user": user == null ? null : user?.toJson(),
  };
}

class Product {
  Product({
    this.id,
    this.name,
    this.slug,
    this.categoriesId,
    this.price,
    this.description,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? name;
  String? slug;
  int? categoriesId;
  int? price;
  String? description;
  dynamic? deletedAt;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    slug: json["slug"] == null ? null : json["slug"],
    categoriesId: json["categories_id"] == null ? null : json["categories_id"],
    price: json["price"] == null ? null : json["price"],
    description: json["description"] == null ? null : json["description"],
    deletedAt: json["deleted_at"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "slug": slug == null ? null : slug,
    "categories_id": categoriesId == null ? null : categoriesId,
    "price": price == null ? null : price,
    "description": description == null ? null : description,
    "deleted_at": deletedAt,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
  };
}

class User {
  User({
    this.id,
    this.name,
    this.email,
    this.emailVerifiedAt,
    this.createdAt,
    this.updatedAt,
    this.addressOne,
    this.addressTwo,
    this.provincesId,
    this.regenciesId,
    this.zipCode,
    this.phoneNumber,
    this.roles,
  });

  int? id;
  String? name;
  String? email;
  dynamic? emailVerifiedAt;
  DateTime? createdAt;
  DateTime? updatedAt;
  dynamic? addressOne;
  dynamic? addressTwo;
  dynamic? provincesId;
  dynamic? regenciesId;
  dynamic? zipCode;
  dynamic? phoneNumber;
  String? roles;

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    email: json["email"] == null ? null : json["email"],
    emailVerifiedAt: json["email_verified_at"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    addressOne: json["address_one"],
    addressTwo: json["address_two"],
    provincesId: json["provinces_id"],
    regenciesId: json["regencies_id"],
    zipCode: json["zip_code"],
    phoneNumber: json["phone_number"],
    roles: json["roles"] == null ? null : json["roles"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "email": email == null ? null : email,
    "email_verified_at": emailVerifiedAt,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
    "address_one": addressOne,
    "address_two": addressTwo,
    "provinces_id": provincesId,
    "regencies_id": regenciesId,
    "zip_code": zipCode,
    "phone_number": phoneNumber,
    "roles": roles == null ? null : roles,
  };
}
