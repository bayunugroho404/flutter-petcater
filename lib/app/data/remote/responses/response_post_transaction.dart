// To parse this JSON data, do
//
//     final responsePostTransaction = responsePostTransactionFromJson(jsonString);

import 'dart:convert';

ResponsePostTransaction responsePostTransactionFromJson(String str) => ResponsePostTransaction.fromJson(json.decode(str));

String responsePostTransactionToJson(ResponsePostTransaction data) => json.encode(data.toJson());

class ResponsePostTransaction {
  ResponsePostTransaction({
    this.success,
    this.data,
    this.message,
  });

  bool? success;
  Data? data;
  String? message;

  factory ResponsePostTransaction.fromJson(Map<String, dynamic> json) => ResponsePostTransaction(
    success: json["success"] == null ? null : json["success"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "success": success == null ? null : success,
    "data": data == null ? null : data?.toJson(),
    "message": message == null ? null : message,
  };
}

class Data {
  Data({
    this.usersId,
    this.shippingPrice,
    this.totalPrice,
    this.paymentMethods,
    this.transactionStatus,
    this.code,
    this.updatedAt,
    this.createdAt,
    this.id,
  });

  int? usersId;
  int? shippingPrice;
  String? totalPrice;
  String? paymentMethods;
  String? transactionStatus;
  String? code;
  DateTime? updatedAt;
  DateTime? createdAt;
  int? id;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    usersId: json["users_id"] == null ? null : json["users_id"],
    shippingPrice: json["shipping_price"] == null ? null : json["shipping_price"],
    totalPrice: json["total_price"] == null ? null : json["total_price"],
    paymentMethods: json["payment_methods"] == null ? null : json["payment_methods"],
    transactionStatus: json["transaction_status"] == null ? null : json["transaction_status"],
    code: json["code"] == null ? null : json["code"],
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    id: json["id"] == null ? null : json["id"],
  );

  Map<String, dynamic> toJson() => {
    "users_id": usersId == null ? null : usersId,
    "shipping_price": shippingPrice == null ? null : shippingPrice,
    "total_price": totalPrice == null ? null : totalPrice,
    "payment_methods": paymentMethods == null ? null : paymentMethods,
    "transaction_status": transactionStatus == null ? null : transactionStatus,
    "code": code == null ? null : code,
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "id": id == null ? null : id,
  };
}
