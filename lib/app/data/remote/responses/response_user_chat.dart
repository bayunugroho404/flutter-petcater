/**
 * Created by Bayu Nugroho
 * Copyright (c) 2021 . All rights reserved.
 */
// To parse this JSON data, do
//
//     final responseUserChat = responseUserChatFromJson(jsonString);

import 'dart:convert';

ResponseUserChat responseUserChatFromJson(String str) => ResponseUserChat.fromJson(json.decode(str));

String responseUserChatToJson(ResponseUserChat data) => json.encode(data.toJson());

class ResponseUserChat {
  ResponseUserChat({
    this.success,
    this.data,
  });

  bool? success;
  List<Datum>? data;

  factory ResponseUserChat.fromJson(Map<String, dynamic> json) => ResponseUserChat(
    success: json["success"] == null ? null : json["success"],
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "success": success == null ? null : success,
    "data": data == null ? null : List<dynamic>.from(data!.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.id,
    this.name,
    this.email,
    this.photo,
    this.emailVerifiedAt,
    this.createdAt,
    this.updatedAt,
    this.addressOne,
    this.addressTwo,
    this.provincesId,
    this.regenciesId,
    this.zipCode,
    this.os_id,
    this.phoneNumber,
    this.roles,
    this.latestMessage,
  });

  int? id;
  String? name;
  String? email;
  String? photo;
  dynamic? emailVerifiedAt;
  DateTime? createdAt;
  DateTime? updatedAt;
  String? addressOne;
  String? addressTwo;
  int? provincesId;
  int? regenciesId;
  int? zipCode;
  String? os_id;
  String? phoneNumber;
  String? roles;
  LatestMessage? latestMessage;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    email: json["email"] == null ? null : json["email"],
    photo: json["photo"] == null ? null : json["photo"],
    emailVerifiedAt: json["email_verified_at"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    addressOne: json["address_one"] == null ? null : json["address_one"],
    addressTwo: json["address_two"] == null ? null : json["address_two"],
    provincesId: json["provinces_id"] == null ? null : json["provinces_id"],
    regenciesId: json["regencies_id"] == null ? null : json["regencies_id"],
    zipCode: json["zip_code"] == null ? null : json["zip_code"],
    os_id: json["os_id"] == null ? null : json["os_id"],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    roles: json["roles"] == null ? null : json["roles"],
    latestMessage: json["latest_message"] == null ? null : LatestMessage.fromJson(json["latest_message"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "email": email == null ? null : email,
    "photo": photo == null ? null : photo,
    "email_verified_at": emailVerifiedAt,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
    "address_one": addressOne == null ? null : addressOne,
    "address_two": addressTwo == null ? null : addressTwo,
    "provinces_id": provincesId == null ? null : provincesId,
    "regencies_id": regenciesId == null ? null : regenciesId,
    "zip_code": zipCode == null ? null : zipCode,
    "os_id": os_id == null ? null : os_id,
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "roles": roles == null ? null : roles,
    "latest_message": latestMessage == null ? null : latestMessage?.toJson(),
  };
}

class LatestMessage {
  LatestMessage({
    this.id,
    this.senderId,
    this.receiverId,
    this.message,
    this.image,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  int? senderId;
  int? receiverId;
  String? message;
  String? image;
  String? createdAt;
  DateTime? updatedAt;

  factory LatestMessage.fromJson(Map<String, dynamic> json) => LatestMessage(
    id: json["id"] == null ? null : json["id"],
    senderId: json["sender_id"] == null ? null : json["sender_id"],
    receiverId: json["receiver_id"] == null ? null : json["receiver_id"],
    message: json["message"] == null ? null : json["message"],
    image: json["image"] == null ? null : json["image"],
    createdAt: json["created_at"] == null ? null : json["created_at"],
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "sender_id": senderId == null ? null : senderId,
    "receiver_id": receiverId == null ? null : receiverId,
    "message": message == null ? null : message,
    "image": image == null ? null : image,
    "createdAt": createdAt == null ? null : createdAt,
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
  };
}
