/**
 * Created by Bayu Nugroho
 * Copyright (c) 2021 . All rights reserved.
 */

import 'dart:convert';

ResponseUserAllChat responseUserAllChatFromJson(String str) => ResponseUserAllChat.fromJson(json.decode(str));

String responseUserAllChatToJson(ResponseUserAllChat data) => json.encode(data.toJson());

class ResponseUserAllChat {
  ResponseUserAllChat({
    this.statusCode,
    this.message,
    this.data,
  });

  int? statusCode;
  String? message;
  List<Datum>? data;

  factory ResponseUserAllChat.fromJson(Map<String, dynamic> json) => ResponseUserAllChat(
    statusCode: json["status_code"] == null ? null : json["status_code"],
    message: json["message"] == null ? null : json["message"],
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode == null ? null : statusCode,
    "message": message == null ? null : message,
    "data": data == null ? null : List<dynamic>.from(data!.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.id,
    this.senderId,
    this.receiverId,
    this.message,
    this.image,
    this.createdAt,
    this.updatedAt,
    this.sender,
    this.receiver,
  });

  int? id;
  int? senderId;
  int? receiverId;
  String? message;
  String? image;
  DateTime? createdAt;
  DateTime? updatedAt;
  Receiver? sender;
  Receiver? receiver;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"] == null ? null : json["id"],
    senderId: json["sender_id"] == null ? null : json["sender_id"],
    receiverId: json["receiver_id"] == null ? null : json["receiver_id"],
    message: json["message"] == null ? null : json["message"],
    image: json["image"] == null ? null : json["image"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    sender: json["sender"] == null ? null : Receiver.fromJson(json["sender"]),
    receiver: json["receiver"] == null ? null : Receiver.fromJson(json["receiver"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "sender_id": senderId == null ? null : senderId,
    "receiver_id": receiverId == null ? null : receiverId,
    "message": message == null ? null : message,
    "image": image == null ? null : image,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
    "sender": sender == null ? null : sender?.toJson(),
    "receiver": receiver == null ? null : receiver?.toJson(),
  };
}

class Receiver {
  Receiver({
    this.id,
    this.name,
    this.email,
    this.photo,
    this.emailVerifiedAt,
    this.createdAt,
    this.updatedAt,
    this.addressOne,
    this.addressTwo,
    this.provincesId,
    this.regenciesId,
    this.zipCode,
    this.os_id,
    this.phoneNumber,
    this.roles,
  });

  int? id;
  String? name;
  String? email;
  String? photo;
  dynamic? emailVerifiedAt;
  DateTime? createdAt;
  DateTime? updatedAt;
  String? addressOne;
  dynamic? addressTwo;
  dynamic? provincesId;
  dynamic? regenciesId;
  dynamic? zipCode;
  String? os_id;
  dynamic? phoneNumber;
  String? roles;

  factory Receiver.fromJson(Map<String, dynamic> json) => Receiver(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    email: json["email"] == null ? null : json["email"],
    photo: json["photo"] == null ? null : json["photo"],
    emailVerifiedAt: json["email_verified_at"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    addressOne: json["address_one"] == null ? null : json["address_one"],
    addressTwo: json["address_two"],
    provincesId: json["provinces_id"],
    regenciesId: json["regencies_id"],
    zipCode: json["zip_code"],
    os_id: json["os_id"] == null ? null : json["os_id"],
    phoneNumber: json["phone_number"],
    roles: json["roles"] == null ? null : json["roles"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "email": email == null ? null : email,
    "photo": photo == null ? null : photo,
    "email_verified_at": emailVerifiedAt,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
    "address_one": addressOne == null ? null : addressOne,
    "address_two": addressTwo,
    "provinces_id": provincesId,
    "regencies_id": regenciesId,
    "zip_code": zipCode,
    "os_id": os_id == null ? null : os_id,
    "phone_number": phoneNumber,
    "roles": roles == null ? null : roles,
  };
}
