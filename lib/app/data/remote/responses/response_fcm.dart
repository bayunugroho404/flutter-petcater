/**
 * Created by Bayu Nugroho
 * Copyright (c) 2021 . All rights reserved.
 */
import 'dart:convert';

ResponseFcm responseFcmFromJson(String str) => ResponseFcm.fromJson(json.decode(str));

String responseFcmToJson(ResponseFcm data) => json.encode(data.toJson());

class ResponseFcm {
  ResponseFcm({
    this.multicastId,
    this.success,
    this.failure,
    this.canonicalIds,
    this.results,
  });

  double? multicastId;
  int? success;
  int? failure;
  int? canonicalIds;
  List<Result>? results;

  factory ResponseFcm.fromJson(Map<String, dynamic> json) => ResponseFcm(
    multicastId: json["multicast_id"] == null ? null : json["multicast_id"].toDouble(),
    success: json["success"] == null ? null : json["success"],
    failure: json["failure"] == null ? null : json["failure"],
    canonicalIds: json["canonical_ids"] == null ? null : json["canonical_ids"],
    results: json["results"] == null ? null : List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "multicast_id": multicastId == null ? null : multicastId,
    "success": success == null ? null : success,
    "failure": failure == null ? null : failure,
    "canonical_ids": canonicalIds == null ? null : canonicalIds,
    "results": results == null ? null : List<dynamic>.from(results!.map((x) => x.toJson())),
  };
}

class Result {
  Result({
    this.messageId,
  });

  String? messageId;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    messageId: json["message_id"] == null ? null : json["message_id"],
  );

  Map<String, dynamic> toJson() => {
    "message_id": messageId == null ? null : messageId,
  };
}
