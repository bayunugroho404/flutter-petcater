/**
 * Created by Bayu Nugroho
 * Copyright (c) 2021 . All rights reserved.
 */

import 'dart:convert';

ResponseStatus responseStatusFromJson(String str) => ResponseStatus.fromJson(json.decode(str));

String responseStatusToJson(ResponseStatus data) => json.encode(data.toJson());

class ResponseStatus {
  ResponseStatus({
    this.success,
    this.message,
  });

  bool? success;
  String? message;

  factory ResponseStatus.fromJson(Map<String, dynamic> json) => ResponseStatus(
    success: json["success"] == null ? null : json["success"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "success": success == null ? null : success,
    "message": message == null ? null : message,
  };
}
