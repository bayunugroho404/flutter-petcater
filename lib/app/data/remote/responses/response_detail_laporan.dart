/**
 * Created by Bayu Nugroho
 * Copyright (c) 2021 . All rights reserved.
 */
import 'dart:convert';

ResponseGetDetailLaporan responseGetDetailLaporanFromJson(String str) => ResponseGetDetailLaporan.fromJson(json.decode(str));

String responseGetDetailLaporanToJson(ResponseGetDetailLaporan data) => json.encode(data.toJson());

class ResponseGetDetailLaporan {
  ResponseGetDetailLaporan({
    this.success,
    this.data,
    this.message,
  });

  bool? success;
  List<Datum>? data;
  String? message;

  factory ResponseGetDetailLaporan.fromJson(Map<String, dynamic> json) => ResponseGetDetailLaporan(
    success: json["success"] == null ? null : json["success"],
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "success": success == null ? null : success,
    "data": data == null ? null : List<dynamic>.from(data!.map((x) => x.toJson())),
    "message": message == null ? null : message,
  };
}

class Datum {
  Datum({
    this.id,
    this.transactionsId,
    this.productsId,
    this.price,
    this.code,
    this.createdAt,
    this.updatedAt,
    this.transaction,
    this.product,
  });

  int? id;
  int? transactionsId;
  int? productsId;
  int? price;
  String? code;
  DateTime? createdAt;
  DateTime? updatedAt;
  Transaction? transaction;
  Product? product;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"] == null ? null : json["id"],
    transactionsId: json["transactions_id"] == null ? null : json["transactions_id"],
    productsId: json["products_id"] == null ? null : json["products_id"],
    price: json["price"] == null ? null : json["price"],
    code: json["code"] == null ? null : json["code"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    transaction: json["transaction"] == null ? null : Transaction.fromJson(json["transaction"]),
    product: json["product"] == null ? null : Product.fromJson(json["product"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "transactions_id": transactionsId == null ? null : transactionsId,
    "products_id": productsId == null ? null : productsId,
    "price": price == null ? null : price,
    "code": code == null ? null : code,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
    "transaction": transaction == null ? null : transaction?.toJson(),
    "product": product == null ? null : product?.toJson(),
  };
}

class Product {
  Product({
    this.id,
    this.name,
    this.slug,
    this.categoriesId,
    this.price,
    this.qty,
    this.description,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? name;
  String? slug;
  int? categoriesId;
  int? price;
  int? qty;
  String? description;
  dynamic? deletedAt;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    slug: json["slug"] == null ? null : json["slug"],
    categoriesId: json["categories_id"] == null ? null : json["categories_id"],
    price: json["price"] == null ? null : json["price"],
    qty: json["qty"] == null ? null : json["qty"],
    description: json["description"] == null ? null : json["description"],
    deletedAt: json["deleted_at"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "slug": slug == null ? null : slug,
    "categories_id": categoriesId == null ? null : categoriesId,
    "price": price == null ? null : price,
    "qty": qty == null ? null : qty,
    "description": description == null ? null : description,
    "deleted_at": deletedAt,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
  };
}

class Transaction {
  Transaction({
    this.id,
    this.usersId,
    this.code,
    this.shippingPrice,
    this.totalPrice,
    this.transactionStatus,
    this.paymentMethods,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  int? usersId;
  String? code;
  int? shippingPrice;
  int? totalPrice;
  String? transactionStatus;
  String? paymentMethods;
  dynamic? deletedAt;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory Transaction.fromJson(Map<String, dynamic> json) => Transaction(
    id: json["id"] == null ? null : json["id"],
    usersId: json["users_id"] == null ? null : json["users_id"],
    code: json["code"] == null ? null : json["code"],
    shippingPrice: json["shipping_price"] == null ? null : json["shipping_price"],
    totalPrice: json["total_price"] == null ? null : json["total_price"],
    transactionStatus: json["transaction_status"] == null ? null : json["transaction_status"],
    paymentMethods: json["payment_methods"] == null ? null : json["payment_methods"],
    deletedAt: json["deleted_at"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "users_id": usersId == null ? null : usersId,
    "code": code == null ? null : code,
    "shipping_price": shippingPrice == null ? null : shippingPrice,
    "total_price": totalPrice == null ? null : totalPrice,
    "transaction_status": transactionStatus == null ? null : transactionStatus,
    "payment_methods": paymentMethods == null ? null : paymentMethods,
    "deleted_at": deletedAt,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
  };
}
