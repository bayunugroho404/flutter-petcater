// To parse this JSON data, do
//
//     final responseAllProduct = responseAllProductFromJson(jsonString);

import 'dart:convert';

ResponseAllProduct responseAllProductFromJson(String str) => ResponseAllProduct.fromJson(json.decode(str));

String responseAllProductToJson(ResponseAllProduct data) => json.encode(data.toJson());

class ResponseAllProduct {
  ResponseAllProduct({
    this.currentPage,
    this.data,
    this.firstPageUrl,
    this.from,
    this.lastPage,
    this.lastPageUrl,
    this.links,
    this.nextPageUrl,
    this.path,
    this.perPage,
    this.prevPageUrl,
    this.to,
    this.total,
    this.success,
    this.message,
  });

  int? currentPage;
  List<Datum>? data;
  String? firstPageUrl;
  int? from;
  int? lastPage;
  String? lastPageUrl;
  List<Link>? links;
  String? nextPageUrl;
  String? path;
  int? perPage;
  dynamic? prevPageUrl;
  int? to;
  int? total;
  bool? success;
  String? message;

  factory ResponseAllProduct.fromJson(Map<String, dynamic> json) => ResponseAllProduct(
    currentPage: json["current_page"] == null ? null : json["current_page"],
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    firstPageUrl: json["first_page_url"] == null ? null : json["first_page_url"],
    from: json["from"] == null ? null : json["from"],
    lastPage: json["last_page"] == null ? null : json["last_page"],
    lastPageUrl: json["last_page_url"] == null ? null : json["last_page_url"],
    links: json["links"] == null ? null : List<Link>.from(json["links"].map((x) => Link.fromJson(x))),
    nextPageUrl: json["next_page_url"] == null ? null : json["next_page_url"],
    path: json["path"] == null ? null : json["path"],
    perPage: json["per_page"] == null ? null : json["per_page"],
    prevPageUrl: json["prev_page_url"],
    to: json["to"] == null ? null : json["to"],
    total: json["total"] == null ? null : json["total"],
    success: json["success"] == null ? null : json["success"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "current_page": currentPage == null ? null : currentPage,
    "data": data == null ? null : List<dynamic>.from(data!.map((x) => x.toJson())),
    "first_page_url": firstPageUrl == null ? null : firstPageUrl,
    "from": from == null ? null : from,
    "last_page": lastPage == null ? null : lastPage,
    "last_page_url": lastPageUrl == null ? null : lastPageUrl,
    "links": links == null ? null : List<dynamic>.from(links!.map((x) => x.toJson())),
    "next_page_url": nextPageUrl == null ? null : nextPageUrl,
    "path": path == null ? null : path,
    "per_page": perPage == null ? null : perPage,
    "prev_page_url": prevPageUrl,
    "to": to == null ? null : to,
    "total": total == null ? null : total,
    "success": success == null ? null : success,
    "message": message == null ? null : message,
  };
}

class Datum {
  Datum({
    this.id,
    this.name,
    this.slug,
    this.categoriesId,
    this.price,
    this.qty,
    this.description,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
    this.galleries,
  });

  int? id;
  String? name;
  String? slug;
  int? categoriesId;
  int? price;
  int? qty;
  String? description;
  dynamic? deletedAt;
  DateTime? createdAt;
  DateTime? updatedAt;
  List<Gallery>? galleries;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    slug: json["slug"] == null ? null : json["slug"],
    categoriesId: json["categories_id"] == null ? null : json["categories_id"],
    price: json["price"] == null ? null : json["price"],
    qty: json["qty"] == null ? null : json["qty"],
    description: json["description"] == null ? null : json["description"],
    deletedAt: json["deleted_at"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    galleries: json["galleries"] == null ? null : List<Gallery>.from(json["galleries"].map((x) => Gallery.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "slug": slug == null ? null : slug,
    "categories_id": categoriesId == null ? null : categoriesId,
    "price": price == null ? null : price,
    "qty": qty == null ? null : qty,
    "description": description == null ? null : description,
    "deleted_at": deletedAt,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
    "galleries": galleries == null ? null : List<dynamic>.from(galleries!.map((x) => x.toJson())),
  };
}

class Gallery {
  Gallery({
    this.id,
    this.photos,
    this.productsId,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? photos;
  int? productsId;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory Gallery.fromJson(Map<String, dynamic> json) => Gallery(
    id: json["id"] == null ? null : json["id"],
    photos: json["photos"] == null ? null : json["photos"],
    productsId: json["products_id"] == null ? null : json["products_id"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "photos": photos == null ? null : photos,
    "products_id": productsId == null ? null : productsId,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
  };
}

class Link {
  Link({
    this.url,
    this.label,
    this.active,
  });

  String? url;
  String? label;
  bool? active;

  factory Link.fromJson(Map<String, dynamic> json) => Link(
    url: json["url"] == null ? null : json["url"],
    label: json["label"] == null ? null : json["label"],
    active: json["active"] == null ? null : json["active"],
  );

  Map<String, dynamic> toJson() => {
    "url": url == null ? null : url,
    "label": label == null ? null : label,
    "active": active == null ? null : active,
  };
}
