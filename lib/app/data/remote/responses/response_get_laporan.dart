// To parse this JSON data, do
//
//     final responseGetLaporan = responseGetLaporanFromJson(jsonString);

import 'dart:convert';

ResponseGetLaporan responseGetLaporanFromJson(String str) => ResponseGetLaporan.fromJson(json.decode(str));

String responseGetLaporanToJson(ResponseGetLaporan data) => json.encode(data.toJson());

class ResponseGetLaporan {
  ResponseGetLaporan({
    this.success,
    this.data,
    this.message,
  });

  bool? success;
  List<Datum>? data;
  String? message;

  factory ResponseGetLaporan.fromJson(Map<String, dynamic> json) => ResponseGetLaporan(
    success: json["success"] == null ? null : json["success"],
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "success": success == null ? null : success,
    "data": data == null ? null : List<dynamic>.from(data!.map((x) => x.toJson())),
    "message": message == null ? null : message,
  };
}

class Datum {
  Datum({
    this.id,
    this.usersId,
    this.code,
    this.shippingPrice,
    this.totalPrice,
    this.transactionStatus,
    this.paymentMethods,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
    this.user,
  });

  int? id;
  int? usersId;
  String? code;
  int? shippingPrice;
  int? totalPrice;
  String? transactionStatus;
  String? paymentMethods;
  dynamic? deletedAt;
  DateTime? createdAt;
  DateTime? updatedAt;
  User? user;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"] == null ? null : json["id"],
    usersId: json["users_id"] == null ? null : json["users_id"],
    code: json["code"] == null ? null : json["code"],
    shippingPrice: json["shipping_price"] == null ? null : json["shipping_price"],
    totalPrice: json["total_price"] == null ? null : json["total_price"],
    transactionStatus: json["transaction_status"] == null ? null : json["transaction_status"],
    paymentMethods: json["payment_methods"] == null ? null : json["payment_methods"],
    deletedAt: json["deleted_at"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    user: json["user"] == null ? null : User.fromJson(json["user"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "users_id": usersId == null ? null : usersId,
    "code": code == null ? null : code,
    "shipping_price": shippingPrice == null ? null : shippingPrice,
    "total_price": totalPrice == null ? null : totalPrice,
    "transaction_status": transactionStatus == null ? null : transactionStatus,
    "payment_methods": paymentMethods == null ? null : paymentMethods,
    "deleted_at": deletedAt,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
    "user": user == null ? null : user?.toJson(),
  };
}

class User {
  User({
    this.id,
    this.name,
    this.email,
    this.photo,
    this.emailVerifiedAt,
    this.createdAt,
    this.updatedAt,
    this.addressOne,
    this.addressTwo,
    this.provincesId,
    this.regenciesId,
    this.zipCode,
    this.phoneNumber,
    this.roles,
  });

  int? id;
  String? name;
  String? email;
  String? photo;
  dynamic? emailVerifiedAt;
  DateTime? createdAt;
  DateTime? updatedAt;
  String? addressOne;
  String? addressTwo;
  int? provincesId;
  int? regenciesId;
  int? zipCode;
  String? phoneNumber;
  String? roles;

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    email: json["email"] == null ? null : json["email"],
    photo: json["photo"] == null ? null : json["photo"],
    emailVerifiedAt: json["email_verified_at"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    addressOne: json["address_one"] == null ? null : json["address_one"],
    addressTwo: json["address_two"] == null ? null : json["address_two"],
    provincesId: json["provinces_id"] == null ? null : json["provinces_id"],
    regenciesId: json["regencies_id"] == null ? null : json["regencies_id"],
    zipCode: json["zip_code"] == null ? null : json["zip_code"],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    roles: json["roles"] == null ? null : json["roles"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "email": email == null ? null : email,
    "photo": photo == null ? null : photo,
    "email_verified_at": emailVerifiedAt,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
    "address_one": addressOne == null ? null : addressOne,
    "address_two": addressTwo == null ? null : addressTwo,
    "provinces_id": provincesId == null ? null : provincesId,
    "regencies_id": regenciesId == null ? null : regenciesId,
    "zip_code": zipCode == null ? null : zipCode,
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "roles": roles == null ? null : roles,
  };
}
