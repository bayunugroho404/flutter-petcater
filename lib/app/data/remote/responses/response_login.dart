// To parse this JSON data, do
//
//     final responseLogin = responseLoginFromJson(jsonString);

import 'dart:convert';

ResponseLogin responseLoginFromJson(String str) => ResponseLogin.fromJson(json.decode(str));

String responseLoginToJson(ResponseLogin data) => json.encode(data.toJson());

class ResponseLogin {
  ResponseLogin({
    this.statusCode,
    this.message,
    this.token,
    this.data,
    this.error,
  });

  int? statusCode;
  String? message;
  String? token;
  Data? data;
  String? error;

  factory ResponseLogin.fromJson(Map<String, dynamic> json) => ResponseLogin(
    statusCode: json["status_code"] == null ? null : json["status_code"],
    message: json["message"] == null ? null : json["message"],
    token: json["token"] == null ? null : json["token"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
    error: json["error"] == null ? null : json["error"],
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode == null ? null : statusCode,
    "message": message == null ? null : message,
    "token": token == null ? null : token,
    "data": data == null ? null : data?.toJson(),
    "error": error == null ? null : error,
  };
}

class Data {
  Data({
    this.id,
    this.name,
    this.email,
    this.emailVerifiedAt,
    this.createdAt,
    this.updatedAt,
    this.addressOne,
    this.addressTwo,
    this.provincesId,
    this.regenciesId,
    this.zipCode,
    this.phoneNumber,
    this.roles,
  });

  int? id;
  String? name;
  String? email;
  dynamic? emailVerifiedAt;
  DateTime? createdAt;
  DateTime? updatedAt;
  dynamic? addressOne;
  dynamic? addressTwo;
  dynamic? provincesId;
  dynamic? regenciesId;
  dynamic? zipCode;
  dynamic? phoneNumber;
  String? roles;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    email: json["email"] == null ? null : json["email"],
    emailVerifiedAt: json["email_verified_at"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    addressOne: json["address_one"],
    addressTwo: json["address_two"],
    provincesId: json["provinces_id"],
    regenciesId: json["regencies_id"],
    zipCode: json["zip_code"],
    phoneNumber: json["phone_number"],
    roles: json["roles"] == null ? null : json["roles"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "email": email == null ? null : email,
    "email_verified_at": emailVerifiedAt,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
    "address_one": addressOne,
    "address_two": addressTwo,
    "provinces_id": provincesId,
    "regencies_id": regenciesId,
    "zip_code": zipCode,
    "phone_number": phoneNumber,
    "roles": roles == null ? null : roles,
  };
}
