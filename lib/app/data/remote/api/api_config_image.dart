import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:image_picker/image_picker.dart';
import 'package:petcater/app/data/remote/responses/response_status.dart';
import 'package:petcater/utils/constant.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2021 . All rights reserved.
 */
class ApiServiceImage {
  Future<ResponseStatus> postChat(
      String token,
      PickedFile image,
      String sender_id,
      String receiver_id) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    var request =
        http.MultipartRequest("POST", Uri.parse("$BASE_URL/api/chat/add"));
    request.headers.addAll(headers);
    request.fields["sender_id"] = sender_id;
    request.fields["receiver_id"] = receiver_id;
    var pic = await http.MultipartFile.fromPath("image", image.path);
    request.files.add(pic);
    var response = await request.send();
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    if (response.statusCode == 200) {
      return ResponseStatus.fromJson(json.decode(responseString));
    } else {
      throw Exception('gagal');
    }
  }
}
