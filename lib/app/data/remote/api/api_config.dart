import 'dart:convert';

import 'package:http/http.dart' show Client, Response;
import 'package:petcater/app/data/remote/responses/product_by_category.dart';
import 'package:petcater/app/data/remote/responses/response_category.dart';
import 'package:petcater/app/data/remote/responses/response_detail_laporan.dart';
import 'package:petcater/app/data/remote/responses/response_fcm.dart';
import 'package:petcater/app/data/remote/responses/response_get_cart.dart';
import 'package:petcater/app/data/remote/responses/response_get_laporan.dart';
import 'package:petcater/app/data/remote/responses/response_get_transaction.dart';
import 'package:petcater/app/data/remote/responses/response_login.dart';
import 'package:petcater/app/data/remote/responses/response_post_cart.dart';
import 'package:petcater/app/data/remote/responses/response_post_transaction.dart';
import 'package:petcater/app/data/remote/responses/response_product.dart';
import 'package:petcater/app/data/remote/responses/response_register.dart';
import 'package:petcater/app/data/remote/responses/response_status.dart';
import 'package:petcater/app/data/remote/responses/response_user_all_chat.dart';
import 'package:petcater/app/data/remote/responses/response_user_chat.dart';
import 'package:petcater/utils/constant.dart';

class APIService {
  Client client = Client();
  String base_url = BASE_URL;
  String base_url_fcm = BASE_URL_FCM;

  Future<ResponseRegister> postRegister(
    String name,
    String email,
    String password,
    String password_confirmation,
    String address,
  ) async {
    Response response;
    response = await client.post(Uri.parse("$base_url/api/register"), body: {
      "name": name,
      "email": email,
      "password": password,
      "password_confirmation": password_confirmation,
      "address_one": address,
    });
    print("saads " + response.body);
    if (response.statusCode == 200 || response.statusCode == 201) {
      return ResponseRegister.fromJson(json.decode(response.body));
    } else {
      throw Exception('${response.body}');
    }
  }

  Future<ResponsePostCart> postCart(
    String token,
    String products_id,
    String qty,
  ) async {
    Response response;
    response = await client.post(Uri.parse("$base_url/api/cart"), body: {
      "qty": qty,
      "products_id": products_id,
    }, headers: {
      "Authorization": "Bearer $token"
    });

    if (response.statusCode == 200 || response.statusCode == 201) {
      return ResponsePostCart.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponseUserChat> getChat(
    String token,
    String role,
  ) async {
    Response response;
    response = await client.post(Uri.parse("$base_url/api/chat/user"), body: {
      "role": role,
    }, headers: {
      "Authorization": "Bearer $token"
    });
    print(response.body);
    if (response.statusCode == 200 || response.statusCode == 201) {
      return ResponseUserChat.fromJson(json.decode(response.body));
    } else {
      throw Exception('${response.body}');
    }
  }


  Future<ResponseStatus> sendChat(
    String token,
    String sender_id,
    String receiver_id,
    String message,
  ) async {
    Response response;
    response = await client.post(Uri.parse("$base_url/api/chat/add"), body: {
      "sender_id": sender_id,
      "receiver_id": receiver_id,
      "message": message,
    }, headers: {
      "Authorization": "Bearer $token"
    });
    print(response.body);
    if (response.statusCode == 200 || response.statusCode == 201) {
      return ResponseStatus.fromJson(json.decode(response.body));
    } else {
      throw Exception('${response.body}');
    }
  }

  Future<ResponseUserAllChat> getAllChat(
    String token,
    String sender_id,
    String receiver_id,
  ) async {
    Response response;
    response = await client.post(Uri.parse("$base_url/api/chat/all"), body: {
      "sender_id": sender_id,
      "receiver_id": receiver_id,
    }, headers: {
      "Authorization": "Bearer $token"
    });
    if (response.statusCode == 200 || response.statusCode == 201) {
      return ResponseUserAllChat.fromJson(json.decode(response.body));
    } else {
      throw Exception('${response.body}');
    }
  }

  Future<ResponseStatus> updateProfile(
    String token,
    String name,
    String email,
    String address,
  ) async {
    Response response;
    response = await client.post(Uri.parse("$base_url/api/user/update"), body: {
      "name": name,
      "email": email,
      "address_one": address,
    }, headers: {
      "Authorization": "Bearer $token"
    });

    print("saads " + response.body);
    if (response.statusCode == 200 || response.statusCode == 201) {
      return ResponseStatus.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponsePostTransaction> postTransaction(
    String token,
    String total_price,
  ) async {
    Response response;
    response = await client.post(Uri.parse("$base_url/api/transaction"), body: {
      "payment_methods": "Transfer",
      "total_price": total_price,
    }, headers: {
      "Authorization": "Bearer $token"
    });
    if (response.statusCode == 200 || response.statusCode == 201) {
      return ResponsePostTransaction.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponseLogin> postLogin(
    String email,
    String password,
    String os_id,
  ) async {
    Response response;
    response = await client.post(Uri.parse("$base_url/api/login"), body: {
      "email": email,
      "password": password,
      "os_id": os_id,
    });
    print("response " + response.body);
    if (response.statusCode == 200) {
      return ResponseLogin.fromJson(json.decode(response.body));
    } else {
      throw Exception('${response.body}');
    }
  }

  Future<ResponseCategory> getCategory() async {
    Response response;
    response = await client.get(Uri.parse("$base_url/api/category"));
    print("response " + response.body);
    if (response.statusCode == 200) {
      return ResponseCategory.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponseGetLaporan> getLaporan(String token) async {
    Response response;
    response = await client.get(Uri.parse("$base_url/api/transactions"),
        headers: {"Authorization": "Bearer $token"});
    if (response.statusCode == 200) {
      return ResponseGetLaporan.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponseGetDetailLaporan> getDetailLaporan(
      String token, String id) async {
    Response response;
    response = await client.get(Uri.parse("$base_url/api/transaction/$id"),
        headers: {"Authorization": "Bearer $token"});
    if (response.statusCode == 200) {
      return ResponseGetDetailLaporan.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponseGetTransaction> getTransaction(String token) async {
    Response response;
    response = await client.get(
        Uri.parse(
          "$base_url/api/transaction",
        ),
        headers: {"Authorization": "Bearer $token"});
    print(response.body);
    if (response.statusCode == 200) {
      return ResponseGetTransaction.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponseAllProduct> getProduct() async {
    Response response;
    response = await client.get(Uri.parse("$base_url/api/product"));
    print("response " + response.body);
    if (response.statusCode == 200) {
      return ResponseAllProduct.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponseGetCart> getCart(String token) async {
    Response response;
    response = await client.get(Uri.parse("$base_url/api/cart"),
        headers: {"Authorization": "Bearer $token"});
    print("response " + response.body);
    if (response.statusCode == 200) {
      return ResponseGetCart.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponseProductByCategory> getProductByCategory(String id) async {
    Response response;
    response = await client.get(Uri.parse("$base_url/api/category/$id"));
    print("response " + response.body);
    if (response.statusCode == 200) {
      return ResponseProductByCategory.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }


  Future<ResponseFcm> sendNotif(String body) async {
    Response response;
    response = await client.post(Uri.parse("$base_url_fcm"),
        body:body,
        headers: {
          "Content-Type" : "application/json",
          "Authorization" : "$SERVER_KEY",
        });
    print(response.body);
    if (response.statusCode == 200) {
      return ResponseFcm.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }
}
