import 'package:petcater/app/data/remote/responses/response_product.dart';
import 'package:petcater/app/data/remote/responses/product_by_category.dart' as pc;

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2021 . All rights reserved.
 */

class CategoryModel{
  int? id;
  String? name;
  String? image;

  CategoryModel({this.id, this.name, this.image});
}

class ProductModel{
  int? id;
  String? name;
  String? image;
  List<Gallery>?imageGallery;
  String? price;
  String? desc;

  ProductModel({this.id, this.name, this.image,this.price,this.desc,this.imageGallery});
}

class CartModel{
  int? id;
  String? name;
  String? image;
  int? price;
  int? qty;



  CartModel({this.id, this.name, this.image,this.price,this.qty});

}

class DetailChatModel{
  String? id;
  String? name;
  String? image;
  String? os_id;
  DateTime? time;

  DetailChatModel({this.id, this.name, this.image,this.time,this.os_id});

}


class ProductCategory{
  int? id;
  String? name;
  String? image;
  String? price;
  String? desc;
  List<pc.Gallery>?imageGallery;

  ProductCategory({this.id, this.name, this.image,this.price,this.desc,this.imageGallery});
}

class ChatModel{
  String? name;
  int? id;
  String? image;
  String? lastChat;
  String? date;
  DateTime? dateSort;
  String? lastMessage;
  String? os_id;

  ChatModel({this.name, this.id, this.image, this.lastChat,this.date,this.lastMessage,this.dateSort,this.os_id});
}

class HistoryModel{
  String? name;
  String? status;
  String? date;
  int? id;

  HistoryModel({this.name, this.status, this.date, this.id});
}